//
//  Information+Additions.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Information+Additions.h"
#import "TTInformationCell.h"

@implementation Information (Additions)

+ (NSArray *)groupNames
{
    NSMutableArray *groupNames = [NSMutableArray new];
    NSArray *items = [Information MR_findAll];
    for (Information *item in items) {
        if ([groupNames containsObject:item.group]) {
            continue;
        }
        [groupNames addObject:item.group];
    }
    [groupNames sortUsingSelector:@selector(compare:)];
    return groupNames;
}

- (Class)cellClass
{
    return [TTInformationCell class];
}

@end
