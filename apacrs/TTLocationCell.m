//
//  TTLocationCell.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/16/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTLocationCell.h"
#import "Venue.h"

@implementation TTLocationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    if ([object isKindOfClass:[Venue class]]) {
        Venue *venue = (Venue *)object;
        self.textLabel.text = venue.exhibitorName;
    }
    return YES;
}

@end
