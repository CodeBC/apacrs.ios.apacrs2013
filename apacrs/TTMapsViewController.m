//
//  TTMapsViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 6/1/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTMapsViewController.h"
#import "TTTitleCell.h"
#import "TTMapViewController.h"

#import "TTAppTheme.h"

@interface TTMapsViewController ()

@end

@implementation TTMapsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.cellFactory mapObjectClass:[NITitleCellObject class] toCellClass:[TTTitleCell class]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Maps", nil);
    
    NSArray *items = @[
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"Area Map of Suntec Convention Ctr", nil)],
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"L3 Suntec Convention Ctr", nil)],
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"L4 Suntec Convention Ctr", nil)]
                       ];
    self.tableModel = [[NITableViewModel alloc] initWithListArray:items delegate:self.cellFactory];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITitleCellObject *titleObject = [self.tableModel objectAtIndexPath:indexPath];
    TTMapViewController *mapViewController = [[TTMapViewController alloc] init];
    mapViewController.title = titleObject.title;
    switch (indexPath.row) {
        case 0:
            mapViewController.image = [UIImage imageNamed:@"suntac.jpg"];
            break;
        case 1:
            mapViewController.image = [UIImage imageNamed:@"Level 3.jpg"];
            break;
        case 2:
            mapViewController.image = [UIImage imageNamed:@"Level 4.jpg"];
            break;
    }
    [self.navigationController pushViewController:mapViewController animated:YES];
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView * childView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 180)];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 240, 180)];
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.font = [TTAppTheme regularFontWithSize:15];
    textLabel.shadowColor = [UIColor colorWithWhite:1.0 alpha:0.8];
    textLabel.numberOfLines = 5;
    textLabel.textAlignment = NSTextAlignmentCenter;
    
    textLabel.text = @"Nearest MRT station is Esplanade station (CC3)";
    [childView addSubview:textLabel];
    
    return childView;
}

@end
