//
//  TTFilmFestivalDetailViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/29/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTHTMLTemplateViewController.h"

@class FilmFestival;

@interface TTFilmFestivalDetailViewController : TTHTMLTemplateViewController

@property (nonatomic, strong) FilmFestival *filmFestival;

@end
