//
//  FilmFestival.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 6/21/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "FilmFestival.h"
#import "Speaker.h"


@implementation FilmFestival

@dynamic category;
@dynamic categoryId;
@dynamic id;
@dynamic number;
@dynamic synopsis;
@dynamic title;
@dynamic isHidden;
@dynamic speaker;

@end
