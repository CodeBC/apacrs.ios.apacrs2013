//
//  TTInformationViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTTableViewController.h"

@interface TTInformationViewController : TTTableViewController

@property (nonatomic, strong) NSString *group;

@end
