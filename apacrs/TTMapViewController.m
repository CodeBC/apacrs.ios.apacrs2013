//
//  TTMapViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 6/1/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTMapViewController.h"

@interface TTMapViewController ()

@property (nonatomic, strong) NIPhotoScrollView *photoView;

@end

@implementation TTMapViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    self.photoView = [[NIPhotoScrollView alloc] initWithFrame:self.view.bounds];
    self.photoView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.photoView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.image) {
        [self.photoView setImage:self.image photoSize:NIPhotoScrollViewPhotoSizeOriginal];
    }
}

@end
