//
//  Article.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/19/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Article.h"


@implementation Article

@dynamic id;
@dynamic title;
@dynamic url;

@end
