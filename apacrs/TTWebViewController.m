//
//  TTWebViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/4/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTWebViewController.h"

@interface TTWebViewController ()

@end

@implementation TTWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0,
                                                               self.view.width,
                                                               self.view.height)];
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.webView];
}

@end
