//
//  MethodSwizzle.h
//  Baydin
//
//  Created by Thant Thet Khin Zaw on 2/18/13.
//  Copyright (c) 2013 Thant Thet Khin Zaw. All rights reserved.
//

#ifndef Baydin_MethodSwizzle_h
#define Baydin_MethodSwizzle_h

void Swizzle(Class c, SEL orig, SEL new);

#endif
