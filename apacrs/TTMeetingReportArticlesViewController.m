//
//  TTMeetingReportArticlesViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTMeetingReportArticlesViewController.h"
#import "Article.h"
#import "TTArticleCell.h"
#import "TTMeetingReportDetailViewController.h"

@interface TTMeetingReportArticlesViewController ()

@end

@implementation TTMeetingReportArticlesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Articles", nil);
        [self.cellFactory mapObjectClass:[Article class] toCellClass:[TTArticleCell class]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *items = [Article MR_findAll];
    NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *sorted = [items sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];
    self.tableModel = [[NITableViewModel alloc] initWithListArray:sorted delegate:self.cellFactory];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Article *article = [self.tableModel objectAtIndexPath:indexPath];
    TTMeetingReportDetailViewController *viewController = [[TTMeetingReportDetailViewController alloc] init];
    viewController.request = [NSURLRequest requestWithURL:[NSURL URLWithString:article.url]];
    viewController.title = article.title;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
