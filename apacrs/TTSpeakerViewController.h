//
//  TTSpeakerViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/16/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTProgramsViewController.h"

@class Speaker;

@interface TTSpeakerViewController : TTProgramsViewController

@property (nonatomic, strong) Speaker *speaker;
@property (nonatomic, assign) BOOL isBookmarked;

@end
