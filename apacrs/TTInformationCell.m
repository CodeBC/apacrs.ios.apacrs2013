//
//  TTInformationCell.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTInformationCell.h"
#import "Information.h"
#import "TTAppTheme.h"

#define TITLE_HEIGHT 25.0f
#define CONTACT_HEIGHT 30.0f
#define DESCRIPTION_HEIGHT 50.0f
#define PADDING 10.0f

@implementation TTInformationCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = [TTAppTheme cellBackgroundColor];
        
        self.titleLabel = [[UILabel alloc] init];
        self.descriptionLabel = [[UILabel alloc] init];
        self.contactLabel = [[UILabel alloc] init];
        
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.descriptionLabel];
        [self.contentView addSubview:self.contactLabel];
        
        self.titleLabel.font = [TTAppTheme mediumFontWithSize:14];
        self.descriptionLabel.font = [TTAppTheme regularFontWithSize:12.0f];
        self.contactLabel.font = [TTAppTheme mediumFontWithSize:16.0f];
        
        self.titleLabel.textColor = [TTAppTheme darkColor];
        self.contactLabel.textColor = [TTAppTheme darkColor];
        
        self.descriptionLabel.numberOfLines = 0;
        
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.descriptionLabel.backgroundColor = [UIColor clearColor];
        self.contactLabel.backgroundColor = [UIColor clearColor];
        
        self.contactLabel.textAlignment = NSTextAlignmentRight;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    UIColor *color;
    if (selected) {
        color = [UIColor whiteColor];
    } else {
        color = [UIColor blackColor];
    }
    self.titleLabel.textColor = [TTAppTheme darkColor];
    self.descriptionLabel.textColor = color;
    self.contactLabel.textColor = [TTAppTheme darkColor];
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    self.item = (Information *)object;
    if ([self.item isKindOfClass:[Information class]]) {
        self.titleLabel.text = self.item.title;
        self.descriptionLabel.text = [self.item.desc stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
        self.contactLabel.text = self.item.contact;
    }
    return YES;
}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    Information *myItem = (Information *) object;
    if([myItem isKindOfClass:[Information class]]) {
        CGSize maximumSize = CGSizeMake(300, 600);
        
        CGSize strSize = [myItem.desc sizeWithFont:[UIFont systemFontOfSize:12.0f] constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
        
        return TITLE_HEIGHT + (10+strSize.height+10) + PADDING * 2;
    }
    return TITLE_HEIGHT + DESCRIPTION_HEIGHT + PADDING * 2;
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGSize maximumSize = CGSizeMake(300, 600);
    CGSize strSize = [self.item.desc sizeWithFont:[UIFont systemFontOfSize:12.0f] constrainedToSize:maximumSize lineBreakMode:NSLineBreakByWordWrapping];
    
    self.titleLabel.frame = CGRectMake(5.0, PADDING, 250.0, TITLE_HEIGHT);
    self.descriptionLabel.frame = CGRectMake(5.0, CGRectGetMaxY(self.titleLabel.frame), 160, (10+strSize.height+10) );
    self.contactLabel.frame = CGRectMake(self.contentView.width - 140.0f, self.contentView.center.y - CONTACT_HEIGHT/2, 120.0f, CONTACT_HEIGHT);
}

@end
