//
//  UINavigationController+StatusBarStyle.m
//  EasyNews
//
//  Created by Thant Thet Khin Zaw on 5/5/14.
//  Copyright (c) 2014 myOpenware. All rights reserved.
//

#import "UINavigationController+StatusBarStyle.h"

@implementation UINavigationController (StatusBarStyle)

//- (UIViewController *)childViewControllerForStatusBarStyle
//{
//    return self;
//}
//
//- (UIViewController *)childViewControllerForStatusBarHidden
//{
//    return self.visibleViewController;
//}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
