//
//  TTProgramsMenuCell.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/30/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTProgramsMenuCell.h"
#import "TTAppTheme.h"

@implementation TTProgramsMenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIImageView *arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ForwardArrow"]];
        self.accessoryView = arrowView;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    NITitleCellObject *titleObject = (NITitleCellObject *)object;
    self.textLabel.text = titleObject.title;
    return YES;
}

@end
