//
//  Speaker+Additions.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Speaker+Additions.h"
#import "NSArray+TTAddition.h"

@implementation Speaker (Additions)

- (NSComparisonResult)compare:(Speaker *)anotherSpeaker usingSortOrder:(NSArray *)sortOrder
{
    NSString *sid1 = [self.id stringValue];
    NSString *sid2 = [anotherSpeaker.id stringValue];
    if ([sortOrder containsObject:sid1] && [sortOrder containsObject:sid2]) {
        if ([sortOrder indexOfObject:sid1] < [sortOrder indexOfObject:sid2]) {
            return NSOrderedAscending;
        } else if ([sortOrder indexOfObject:sid1] > [sortOrder indexOfObject:sid2]) {
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }
    return NSOrderedSame;
}

@end

@implementation NSArray (SpeakerAddition)

- (NSArray *)groupedSpeakerByName
{
    NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *sorted = [self sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];
    return [sorted groupedByKeyPath:@"name"];
}

- (NSArray *)groupedSpeakerByCountry
{
    return [self groupedByKeyPath:@"country"];
}

- (NSArray *)speakerNamesWithFormatString:(NSString *)format
{
    NSMutableArray *names = [NSMutableArray new];
    for (Speaker *speaker in self) {
        [names addObject:[NSString stringWithFormat:format, speaker.name]];
    }
    return names;
}

@end
