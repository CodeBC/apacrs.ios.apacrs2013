//
//  TTSpeakerView.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/28/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Speaker;

@interface TTSpeakerView : UIView

@property (nonatomic, strong) UIImageView *speakerImageView;
@property (nonatomic, strong) UILabel *speakerNameLabel;
@property (nonatomic, strong) UILabel *speakerCountryLabel;
@property (nonatomic, strong) Speaker *speaker;

- (id)initWithSpeaker:(Speaker *)speaker;

@end
