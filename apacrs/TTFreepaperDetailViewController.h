//
//  TTFreepaperDetailViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTHTMLTemplateViewController.h"

@class Freepaper;

@interface TTFreepaperDetailViewController : TTHTMLTemplateViewController

@property (nonatomic, strong) Freepaper *freepaper;

@end
