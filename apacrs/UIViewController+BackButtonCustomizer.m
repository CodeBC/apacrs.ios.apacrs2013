//
//  UIViewController+BackButtonCustomizer.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 8/8/14.
//  Copyright (c) 2014 myOpenware. All rights reserved.
//

#import "UIViewController+BackButtonCustomizer.h"
#import "MethodSwizzle.h"
#import "UIImage+Addition.h"
@implementation UIViewController (BackButtonCustomizer)

+ (void)load
{
    Swizzle([UIViewController class], @selector(viewDidLoad), @selector(tt_viewDidLoad));
}

- (void)tt_viewDidLoad
{
    [self tt_viewDidLoad];
    
    UIImage *backButtonImage = [UIImage imageNamed:@"Back"];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:backButtonImage
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:nil
                                                                  action:nil];
    self.navigationItem.backBarButtonItem = backButton;
}

@end
