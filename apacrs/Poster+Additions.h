//
//  Poster+Additions.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/29/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Poster.h"

@interface Poster (Additions)

+ (NSArray *)categoryNames;

@end
