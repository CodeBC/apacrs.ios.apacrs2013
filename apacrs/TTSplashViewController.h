//
//  TTSplashViewController.h
//  apacrs
//
//  Created by Ye Myat Min on 7/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTSplashViewController : UIViewController

@property (nonatomic, strong) UIImageView *photoView;

@end
