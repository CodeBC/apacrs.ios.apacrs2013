//
//  Freepaper+Additions.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Freepaper+Additions.h"
#import "Program+Additions.h"

@implementation Freepaper (Additions)

+ (NSArray *)dateTitles
{
    NSArray *programs = [Freepaper MR_findAll];
    return [programs dateTitles];
}

- (NSString *)dateString
{
    static NSDateFormatter *formatter;
    if (!formatter) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [formatter setDateFormat:@"dd MMM (EEE)"];
    }
    
    return [formatter stringFromDate:self.date];
}

- (NSString *)programTime
{
    NSString *programTime = @"";
    if (self.startTime && self.endTime) {
        programTime = [NSString stringWithFormat:@"%@ - %@hrs", self.startTime, self.endTime];
    } else if (self.startTime) {
        programTime = self.startTime;
        programTime = [programTime stringByAppendingString:@"hrs"];
    }
    return programTime;
}

@end
