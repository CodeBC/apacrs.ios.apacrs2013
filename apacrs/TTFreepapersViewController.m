//
//  TTFreepapersViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTFreepapersViewController.h"

#import "Freepaper.h"
#import "Freepaper+Additions.h"
#import "Time.h"
#import "Time+Additions.h"

#import "NSString+DateFormatAddition.h"
#import "NSArray+TTAddition.h"

#import "TTFreepaperDetailViewController.h"

#import "TTFreepaperCell.h"

@interface TTFreepapersViewController ()

@end

@implementation TTFreepapersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.cellFactory mapObjectClass:[Freepaper class] toCellClass:[TTFreepaperCell class]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Free Papers", nil);
}

#pragma mark - Retrive Titles

- (NSArray *)dateTitles
{
    NSArray *titles = [Freepaper dateTitles];
    NSAssert(titles.count, @"No Date Titles");
    return titles;
}

- (NSArray *)timeTitles
{
    NSString *dateTitle = self.dateChooser.sectionTitles[self.dateChooser.selectedSegmentIndex];
    NSAssert(dateTitle, @"No Date Title is selected");
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %@",
                              @"date", [dateTitle dateValue]];
    
    NSArray *programs = [Freepaper MR_findAllWithPredicate:predicate];
    NSAssert(programs, @"No progams found for %@", dateTitle);
    NSArray *times = [programs arrayOfKeyPath:@"time"];
    NSAssert(times, @"No times are extracted from freepapers");
    NSMutableArray *titles = [[times timeTitles] mutableCopy];
    NSAssert(titles.count, @"No Time Titles");
    [titles insertObject:@"All" atIndex:0];
    return titles;
}

- (NSArray *)programsForSelectedDateAndTime
{
    NSString *dayTitle = self.dateChooser.sectionTitles[self.dateChooser.selectedSegmentIndex];
    NSString *timeTitle = self.timeChooser.sectionTitles[self.timeChooser.selectedSegmentIndex];
    NSPredicate *predicate;
    if ([timeTitle isEqualToString:@"All"]) {
        predicate = [NSPredicate predicateWithFormat:@"%K = %@ AND %K = %@",
                     @"date", [dayTitle dateValue],
                     @"isHidden", [NSNumber numberWithBool:NO]];
    } else {
        predicate = [NSPredicate predicateWithFormat:@"%K = %@ AND %K = %@ AND %K = %@",
                     @"date", [dayTitle dateValue],
                     @"time.name", timeTitle,
                     @"isHidden", [NSNumber numberWithBool:NO]];
    }
    NSArray *programs = [Freepaper MR_findAllWithPredicate:predicate];
    return programs;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITableViewModel *model = (NITableViewModel *)tableView.dataSource;
    TTFreepaperDetailViewController *programDetailViewController = [[TTFreepaperDetailViewController alloc] initWithNibName:nil bundle:nil];
    programDetailViewController.freepaper = [model objectAtIndexPath:indexPath];
    [self.navigationController pushViewController:programDetailViewController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.cellFactory tableView:tableView heightForRowAtIndexPath:indexPath model:tableView.dataSource];
}

@end
