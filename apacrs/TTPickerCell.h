//
//  TTPickerCell.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/7/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TTPickerCellObject;

@interface TTPickerCell : UITableViewCell <NICell>

@property (nonatomic, strong) UIPickerView *valuePicker;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UITextField *displayTextField;
@property (nonatomic, strong) TTPickerCellObject *object;

@end