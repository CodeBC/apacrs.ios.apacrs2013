//
//  TTFloorPlanViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/16/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTFloorPlanViewController.h"

@interface TTFloorPlanViewController () <UIScrollViewDelegate>

@end

@implementation TTFloorPlanViewController

- (id)init
{
    self = [super init];
    if (self) {
        _floorPlanView = [[UIWebView alloc] initWithFrame:CGRectZero];
    }
    return self;
}

- (void)hilightLocation:(Location *)location
{
    
}

#pragma mark -
#pragma mark UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return [scrollView subviews][0];
}

@end
