//
//  Information.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Information.h"


@implementation Information

@dynamic id;
@dynamic desc;
@dynamic title;
@dynamic contact;
@dynamic group;

@end
