//
//  TTEventCell.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTTitleCell.h"

@class Program;

@interface TTProgramCell : TTTitleCell

@property (nonatomic, strong) UILabel *hallLabel;
@property (nonatomic, strong) UILabel *topicLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UIButton *favouriteButton;
@property (nonatomic, strong) Program *program;
@property (nonatomic, assign) BOOL favourite;

@end
