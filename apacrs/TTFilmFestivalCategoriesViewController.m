//
//  TTFilmFestivalCategoriesViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTFilmFestivalCategoriesViewController.h"
#import "TTTitleCell.h"
#import "FilmFestival.h"
#import "FilmFestival+Additions.h"

#import "TTFilmFestivalsViewController.h"
#import "TTAppTheme.h"

@interface TTFilmFestivalCategoriesViewController ()

@end

@implementation TTFilmFestivalCategoriesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.cellFactory mapObjectClass:[NITitleCellObject class] toCellClass:[TTTitleCell class]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Film Festival", nil);
    
    NSArray *categoryNames = [FilmFestival categoryNames];
    NSMutableArray *listItems = [NSMutableArray new];
    
    for (NSString *category in categoryNames) {
        [listItems addObject:[[NITitleCellObject alloc] initWithTitle:category]];
    }
    
    self.tableModel = [[NITableViewModel alloc] initWithListArray:listItems delegate:self.cellFactory];
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITitleCellObject *titleObject = [self.tableModel objectAtIndexPath:indexPath];
    TTFilmFestivalsViewController *festivalsViewController = [[TTFilmFestivalsViewController alloc] initWithNibName:nil bundle:nil];
    festivalsViewController.category = titleObject.title;
    [self.navigationController pushViewController:festivalsViewController animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView * childView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 220)];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 280, 220)];
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.font = [TTAppTheme regularFontWithSize:12];
    textLabel.shadowColor = [UIColor colorWithWhite:1.0 alpha:0.8];
    textLabel.numberOfLines = 20;
    textLabel.textAlignment = NSTextAlignmentCenter;
    
    textLabel.text = @"FILM FESTIVAL AWARDS CEREMONY\n& COCKTAIL RECEPTION\n\nFriday 12 July, 15:30hrs\nHall 1, Level 4, Suntec Singapore\n\nThis event is sponsored by\nBAUSCH+LOMB\n\nFilms are also available for viewing at the Film Festival kiosk located in the Exhibition Hall.";
    [childView addSubview:textLabel];
    
    childView.backgroundColor = [[TTAppTheme cellBackgroundColor] colorWithAlphaComponent:0.85];
    
    return childView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 220;
}


@end
