//
//  TTHighlightCell.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/30/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTHighlightCell.h"
#import "Program.h"
#import "Program+Additions.h"
#import "TTFavourites.h"

@implementation TTHighlightCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIButton *accessoryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [accessoryButton setImage:[UIImage imageNamed:@"Fav"] forState:UIControlStateNormal];
        accessoryButton.frame = CGRectMake(0, 0, 40, 40);
        
        [accessoryButton addTarget:self action:@selector(toggleFavorite:) forControlEvents:UIControlEventTouchUpInside];
        
        self.accessoryView = accessoryButton;
        self.favouriteButton = accessoryButton;
        
    }
    return self;
}

- (void)toggleFavorite:(id)sender
{
    self.favourite = !self.favourite;
    [TTFavourites favourite:self.favourite program:self.program];
}

- (void)setFavourite:(BOOL)favourite
{
    _favourite = favourite;
    if (favourite) {
        [self.favouriteButton setImage:[UIImage imageNamed:@"Faved"] forState:UIControlStateNormal];
    } else {
        [self.favouriteButton setImage:[UIImage imageNamed:@"Fav"] forState:UIControlStateNormal];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    self.program = (Program *)object;
    if ([self.program isKindOfClass:[Program class]]) {
        self.textLabel.text = self.program.altName;
        NSMutableArray *programs = [TTFavourites favouritedPrograms];
        if ([programs containsObject:self.program.id]) {
            self.favourite = YES;
        } else {
            self.favourite = NO;
        }
    }
    return YES;
}

@end
