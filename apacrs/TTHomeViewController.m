//
//  TTHomeViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 8/7/14.
//  Copyright (c) 2014 myOpenware. All rights reserved.
//

#import "TTHomeViewController.h"
#import "TTMenuViewCell.h"
#import "TTScrollingBannersView.h"

#import "TTProgramsMenuViewController.h"
#import "TTSpeakersViewController.h"
#import "TTVenueViewController.h"
#import "TTSearchViewController.h"
#import "TTMyScheduleViewController.h"
#import "TTSponsorsViewController.h"
#import "TTHighlightsViewController.h"
#import "TTAboutViewController.h"
#import "TTInformationMenuViewController.h"
#import "TTMeetingReportViewController.h"
#import "TTNextMeetingViewController.h"

#define kModalSeguePushedBackAnimationDuration 0.1
#define kModalSegueBringForwardAnimationDuration 0.3
#define kStatusBarHeight 20.0f

#define TOP_BANNER_HEIGHT 92.0f
#define BOTTOM_BANNER_HEIGHT 35.0f

#define TOP_BANNER_HEIGHT_TALL 133.0f
#define BOTTOM_BANNER_HEIGHT_TALL 70.0f

#define BANNER_ROTATION_INTERVAL 3 //seconds

@interface TTHomeViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, TTScrollingBannersViewDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIImageView *splashImageView;

@end

@implementation TTHomeViewController

- (void)loadView
{
    [super loadView];
    
    [self collectionView];
    
    self.splashImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LaunchImage"]];
    self.splashImageView.frame = [[UIScreen mainScreen] bounds];
    [self.navigationController.view addSubview:self.splashImageView];
    
    self.title = NSLocalizedString(@"Home", nil);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
#ifdef DEBUG
    double delayInSeconds = 0.0;
#else
    double delayInSeconds = 2.0;
#endif
    
    [self.splashImageView performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:delayInSeconds];
}

#pragma mark - Is 4inch

- (BOOL)isTall
{
    CGRect bounds = [[UIScreen mainScreen] bounds];
    CGFloat height = bounds.size.height;
    CGFloat scale = [[UIScreen mainScreen] scale];
    
    return (([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) && ((height * scale) >= 1136));
}

#pragma mark - TTScrollingBannersViewDelegate

- (void)scrollingBannerView:(TTScrollingBannersView *)bannerView didTapBannerAtIndex:(NSUInteger)bannerIndex
{
    [self showAbout];
}

#pragma mark - About

- (void)showAbout
{
    TTAboutViewController *aboutViewController = [[TTAboutViewController alloc] initWithNibName:nil bundle:nil];
    [self presentModalViewControllerInsideNavigationController:aboutViewController];
}

#pragma mark - Collection View

- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumInteritemSpacing = 1;
        flowLayout.minimumLineSpacing = 1;
        flowLayout.itemSize = [self isTall] ? CGSizeMake(105, 100) : CGSizeMake(105, 70);
        flowLayout.sectionInset = UIEdgeInsetsMake(1, 1, 1, 1);
//        if ([self isTall]) {
            flowLayout.headerReferenceSize = CGSizeMake(0, TOP_BANNER_HEIGHT_TALL);
            flowLayout.footerReferenceSize = CGSizeMake(0, BOTTOM_BANNER_HEIGHT_TALL);
//        } else {
//            flowLayout.headerReferenceSize = CGSizeMake(0, TOP_BANNER_HEIGHT);
//            flowLayout.footerReferenceSize = CGSizeMake(0, BOTTOM_BANNER_HEIGHT);
//        }
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height) collectionViewLayout:flowLayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        [_collectionView registerClass:[TTMenuViewCell class] forCellWithReuseIdentifier:@"TTMenuViewCell"];
        [_collectionView registerClass:[TTScrollingBannersView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"scrollingBanner"];
        [_collectionView registerClass:[TTScrollingBannersView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"scrollingBanner"];
        
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}

#pragma DataSource

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    id view = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                        withReuseIdentifier:@"scrollingBanner"
                                                                               forIndexPath:indexPath];
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        TTScrollingBannersView *headerBannerView = view;
        headerBannerView.bannerImages = @[[UIImage imageNamed:@"TopBanner"]];
        headerBannerView.delegate = self;
    } else if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        TTScrollingBannersView *footerBannerView = view;
        footerBannerView.bannerImages = @[
                                          [UIImage imageNamed:@"BottomBanner"],
                                          [UIImage imageNamed:@"BottomBannerPlatinum"]
                                          ];
    }
    return view;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 9;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TTMenuViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TTMenuViewCell" forIndexPath:indexPath];
    if (indexPath.row < 3) {
        cell.backgroundColor = [UIColor colorWithRed:0.404 green:0.706 blue:0.780 alpha:1.000];
    } else if (indexPath.row < 6) {
        cell.backgroundColor = [UIColor colorWithRed:0.204 green:0.608 blue:0.710 alpha:1.000];
    } else {
        cell.backgroundColor = [UIColor colorWithRed:0.004 green:0.510 blue:0.635 alpha:1.000];
    }
    
    NSArray *images = @[
                        [UIImage imageNamed:@"Programs"],
                        [UIImage imageNamed:@"Speakers"],
                        [UIImage imageNamed:@"Exhibition"],
                        [UIImage imageNamed:@"Highlights"],
                        [UIImage imageNamed:@"MySchedule"],
                        [UIImage imageNamed:@"Search"],
                        [UIImage imageNamed:@"MeetingNews"],
                        [UIImage imageNamed:@"Information"],
                        [UIImage imageNamed:@"NextMeeting"]
                        ];
    NSArray *titles = @[
                        @"Programs",
                        @"Speakers",
                        @"Exhibition",
                        @"Highlihgts",
                        @"My Schedule",
                        @"Search",
                        @"Meeting News",
                        @"Information",
                        @"Next Meeting"
                        ];
    
    
    cell.imageView.image = images[indexPath.row];
    cell.titleLabel.text = titles[indexPath.row];
    return cell;
}

#pragma mark - Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *viewController;
    switch (indexPath.row) {
        case 0:
            viewController = [[TTProgramsMenuViewController alloc] init];
            break;
        case 1:
            viewController = [[TTSpeakersViewController alloc] init];
            break;
        case 2:
            viewController = [[TTVenueViewController alloc] init];
            break;
        case 3:
            viewController = [[TTHighlightsViewController alloc] init];
            break;
        case 4:
            viewController = [[TTMyScheduleViewController alloc] init];
            break;
        case 5:
            viewController = [[TTSearchViewController alloc] init];
            break;
        case 6:
            viewController = [[TTMeetingReportViewController alloc] init];
            break;
        case 7:
            viewController = [[TTInformationMenuViewController alloc] init];
            break;
        case 8:
            viewController = [[TTNextMeetingViewController alloc] init];
            break;
        default:
            viewController = [[TTProgramsViewController alloc] init];
            break;
    }
    
    [self presentModalViewControllerInsideNavigationController:viewController];
}

#pragma mark - View Transition

- (void)presentModalViewControllerInsideNavigationController:(UIViewController *)modalViewController
{
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:modalViewController];
    navCon.navigationBar.translucent = NO;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 29, 30)];
    [button setBackgroundImage:[UIImage imageNamed:@"Back"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    modalViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [self presentModalViewController:navCon animated:YES];
}

- (void)presentModalViewController:(UIViewController *)modalViewController animated:(BOOL)animated
{
    self.view.userInteractionEnabled = NO;
    
    [self.navigationController.view addSubview:modalViewController.view];
    
    modalViewController.view.layer.affineTransform = CGAffineTransformMakeScale(0.1, 0.1);
    
    UIViewAnimationOptions options = UIViewAnimationOptionCurveLinear;
    [UIView animateWithDuration:0.3 delay:0 options:options animations:^{
        modalViewController.view.layer.affineTransform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        self.view.userInteractionEnabled = YES;
        [super presentViewController:modalViewController animated:NO completion:NULL];
    }];
    
}

- (void)dismissView
{
    UIImageView *presentingView = NISnapshotViewOfView(self.presentedViewController.view);
    presentingView.contentMode = UIViewContentModeScaleToFill;
    presentingView.frame = [[UIScreen mainScreen] bounds];
    
    [self.navigationController.view addSubview:presentingView];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    UIViewAnimationOptions options = UIViewAnimationOptionCurveEaseIn;
    [UIView animateWithDuration:0.3 delay:0 options:options animations:^{
        presentingView.frame = CGRectMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds), 1, 1);
    } completion:^(BOOL finished) {
        [presentingView removeFromSuperview];
    }];
}

@end
