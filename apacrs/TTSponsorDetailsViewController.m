//
//  TTSponsorDetailsViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/11/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTSponsorDetailsViewController.h"
#import "TTSponsor.h"
#import "TTMultilineTitleCellObject.h"

@interface TTSponsorDetailsViewController () <UITableViewDelegate>

@property (nonatomic, strong) NITableViewModel *tableViewModel;

@end

@implementation TTSponsorDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                               0,
                                                               self.view.width,
                                                               self.view.height - self.navigationController.navigationBar.height)
                                              style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    [self.view addSubview:_tableView];
    
    self.tableView.dataSource = _tableViewModel;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)setSponsor:(TTSponsor *)sponsor
{
    if (_sponsor != sponsor) {
        _sponsor = sponsor;
        
        self.title = sponsor.name;
        
        NSArray *list = @[
//                          @"Title",
//                          [[NITitleCellObject alloc] initWithTitle:sponsor.name],
                          @"Address",
                          [[TTMultilineTitleCellObject alloc] initWithTitle:sponsor.address],
                          @"Fax",
                          [[NITitleCellObject alloc] initWithTitle:sponsor.fax],
                          @"Telephone",
                          [[NITitleCellObject alloc] initWithTitle:sponsor.tel],
                          @"Email",
                          [[NITitleCellObject alloc] initWithTitle:sponsor.email],
                          @"Website",
                          [[NITitleCellObject alloc] initWithTitle:sponsor.web]
                          ];
        _tableViewModel = [[NITableViewModel alloc] initWithSectionedArray:list delegate:(id)[NICellFactory class]];
        self.tableView.dataSource = _tableViewModel;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [NICellFactory tableView:tableView heightForRowAtIndexPath:indexPath model:_tableViewModel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
