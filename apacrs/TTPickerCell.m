//
//  TTPickerCell.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/7/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTPickerCell.h"
#import "TTPickerCellObject.h"

@interface TTPickerCell () <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) NSMutableArray *rows;
@property (nonatomic, assign) NSUInteger selectedIndex;

@end

@implementation TTPickerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.valuePicker = [[UIPickerView alloc] init];
        self.valuePicker.dataSource = self;
        self.valuePicker.delegate = self;
        self.valuePicker.showsSelectionIndicator = YES;
        
        self.textField = [[UITextField alloc] init];
        self.textField.inputView = self.valuePicker;
        self.textField.delegate = self;
        [self.contentView addSubview:self.textField];
        
        self.displayTextField = [[UITextField alloc] init];
        self.displayTextField.hidden = YES;
        [self.contentView addSubview:self.displayTextField];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    self.object = object;
    self.selectedIndex = 0;
    
    self.rows = [NSMutableArray arrayWithArray:self.object.titles];
    [self.rows insertObject:self.object.title atIndex:0];
    
    [self.valuePicker reloadAllComponents];
    
    self.textField.text = self.rows[self.selectedIndex];
    self.textField.textColor = [UIColor lightGrayColor];
    
    return YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGRect frame = CGRectInset(self.contentView.bounds, 10, 10);
    self.textField.frame = frame;
}

#pragma mark -

- (void)hidePicker
{
    [self.textField resignFirstResponder];
}

#pragma mark - 

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.displayTextField.frame = textField.frame;
    self.displayTextField.hidden = NO;
    textField.hidden = YES;
    
    self.displayTextField.text = textField.text;
    self.displayTextField.textColor = textField.textColor;
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.barStyle = UIBarStyleBlackOpaque;
    toolbar.items = @[
                      [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                    target:nil
                                                                    action:nil],
                      [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                       style:UIBarButtonItemStyleDone
                                                      target:self
                                                      action:@selector(hidePicker)]
                      ];
    [toolbar sizeToFit];
    textField.inputAccessoryView = toolbar;
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    textField.hidden = NO;
    self.displayTextField.hidden = YES;
    
    textField.textColor = self.displayTextField.textColor;
    textField.text = self.displayTextField.text;
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.rows.count;
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return self.rows[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.selectedIndex = row;
    self.displayTextField.text = self.rows[row];
    
    if (self.selectedIndex == 0) {
        self.displayTextField.textColor = [UIColor lightGrayColor];
    } else {
        self.displayTextField.textColor = [UIColor blackColor];
    }
    
    if (row == 0) {
        self.object.value = nil;
    } else {
        self.object.value = self.rows[row];
    }
}

@end
