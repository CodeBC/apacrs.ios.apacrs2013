//
//  Program+Additions.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/28/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Program.h"

@interface Program (Additions)

+ (NSArray *)dateTitles;
- (NSString *)dateString;
+ (NSArray *)typeNames;
- (NSString *)programTime;

@end

@interface NSArray (ProgramAdditions)

- (NSArray *)dateTitles;

@end