//
//  TTPosterCategoriesViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/29/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTTableViewController.h"

@interface TTPosterCategoriesViewController : TTTableViewController <UITableViewDelegate>

@end
