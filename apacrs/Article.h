//
//  Article.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/19/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Article : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * url;

@end
