//
//  TTProgramOverviewViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 6/2/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTVenueViewController.h"

@interface TTProgramOverviewViewController : UIViewController

@property (nonatomic, strong) UIWebView *floorPlanView;

@end
