//
//  TTTitleCell.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/29/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTTitleCell : UITableViewCell <NICell>

- (void)updateColorsForState:(BOOL)state;

@end
