//
//  TTEventCell.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTProgramCell.h"
#import "Location.h"
#import "Program.h"
#import "Program+Additions.h"
#import "TTFavourites.h"

#import "TTAppTheme.h"

@interface TTProgramCell ()

@property (nonatomic, strong) UIView *hallBackgroundView;

@end

@implementation TTProgramCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = [TTAppTheme cellBackgroundColor];
        
        _hallLabel = [[UILabel alloc] init];
        _topicLabel = [[UILabel alloc] init];
        _timeLabel = [[UILabel alloc] init];
        
        _hallLabel.font = [TTAppTheme regularCondensedFontWithSize:14];
        _topicLabel.font = [TTAppTheme regularFontWithSize:14];
        _timeLabel.font = [TTAppTheme regularFontWithSize:13];
        
        _hallBackgroundView = [[UIView alloc] init];
        _hallBackgroundView.backgroundColor = [TTAppTheme cellSidebarColor];
        [self.contentView addSubview:_hallBackgroundView];
        
        _hallLabel.backgroundColor = [UIColor clearColor];
        _hallLabel.textAlignment = NSTextAlignmentLeft;
        _hallLabel.textColor = [UIColor whiteColor];
        _hallLabel.numberOfLines = 0;
        
        _timeLabel.textColor = [UIColor grayColor];
        
        _topicLabel.numberOfLines = 0;
        
        _topicLabel.backgroundColor = [UIColor clearColor];
        _timeLabel.backgroundColor = [UIColor clearColor];
        
        UIButton *accessoryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [accessoryButton setImage:[UIImage imageNamed:@"Fav"] forState:UIControlStateNormal];
        accessoryButton.frame = CGRectMake(0, 0, 40, 40);
        
        [accessoryButton addTarget:self action:@selector(toggleFavorite:) forControlEvents:UIControlEventTouchUpInside];
        
        self.accessoryView = accessoryButton;
        self.favouriteButton = accessoryButton;
        
        [self.contentView addSubview:_hallLabel];
        [self.contentView addSubview:_topicLabel];
        [self.contentView addSubview:_timeLabel];
    }
    return self;
}

- (void)toggleFavorite:(id)sender
{
    self.favourite = !self.favourite;
    [TTFavourites favourite:self.favourite program:self.program];
}

- (void)setFavourite:(BOOL)favourite
{
    _favourite = favourite;
    if (favourite) {
        [self.favouriteButton setImage:[UIImage imageNamed:@"Faved"] forState:UIControlStateNormal];
    } else {
        [self.favouriteButton setImage:[UIImage imageNamed:@"Fav"] forState:UIControlStateNormal];
    }
}

- (void)updateColorsForState:(BOOL)state
{
    [super updateColorsForState:state];
    
    self.hallBackgroundView.backgroundColor = state ? [UIColor darkGrayColor] : [TTAppTheme cellSidebarColor];
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    if ([object isKindOfClass:[Program class]]) {
        
        self.program = (Program *)object;
        self.topicLabel.text = [NSString stringWithFormat:@"%@", self.program.title];
        self.hallLabel.text = [NSString stringWithFormat:@"%@", self.program.location.name];
        self.timeLabel.text = self.program.programTime;
        
        NSMutableArray *programs = [TTFavourites favouritedPrograms];
        if ([programs containsObject:self.program.id]) {
            self.favourite = YES;
        } else {
            self.favourite = NO;
        }
    }
    return YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat marginBetween = 5.0f;
    CGFloat hallLabelWidth = 70.0f;
    
    CGFloat height = roundf(self.contentView.height/3);
    
    self.hallLabel.frame = CGRectMake(5, 0, hallLabelWidth - 5, self.contentView.height);
    self.hallBackgroundView.frame = CGRectMake(0, 0, hallLabelWidth, self.contentView.height);
    self.topicLabel.frame = CGRectMake(marginBetween + hallLabelWidth, 0,
                                       self.contentView.width - (marginBetween + hallLabelWidth),
                                       height*2);
    self.timeLabel.frame = CGRectMake(self.topicLabel.left,
                                      self.topicLabel.height,
                                      self.topicLabel.width,
                                      height*1);
}

@end
