//
//  Location.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/25/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Location : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *programs;
@end

@interface Location (CoreDataGeneratedAccessors)

- (void)addProgramsObject:(NSManagedObject *)value;
- (void)removeProgramsObject:(NSManagedObject *)value;
- (void)addPrograms:(NSSet *)values;
- (void)removePrograms:(NSSet *)values;

@end

@interface NSArray (LocationGrouping)

- (NSArray *)groupedLocationByName;

@end
