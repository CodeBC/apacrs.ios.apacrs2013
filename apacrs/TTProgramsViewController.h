//
//  TTProgramsViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/12/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTTableViewController.h"

@class SVSegmentedControl;

@interface TTProgramsViewController : TTTableViewController <UITableViewDelegate>

@property (nonatomic, strong) SVSegmentedControl *dateChooser;
@property (nonatomic, strong) SVSegmentedControl *timeChooser;

@property (nonatomic, strong) UIScrollView *dateScrollView;
@property (nonatomic, strong) UIScrollView *timeScrollView;

@property (nonatomic, strong) UIView *emptyView;

- (NSArray *)dateTitles;
- (NSArray *)timeTitles;
- (NSArray *)programsForSelectedDateAndTime;
- (void)reloadEvents;
- (NSString *)emptyText;
- (CGRect)emptyViewFrame;

@end
