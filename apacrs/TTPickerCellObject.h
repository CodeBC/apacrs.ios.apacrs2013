//
//  TTPickerCellObject.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/7/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TTPickerCellObject : NSObject <NICellObject>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, strong) NSArray *values;
@property (nonatomic, strong) NSString *value;

+ (id)pickerObjectWithTitle:(NSString *)title titles:(NSArray *)titles values:(NSArray *)values;

@end
