//
//  TTAppDelegate.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/12/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTAppDelegate.h"

#import "TTAppTheme.h"
#import "UIImage+Addition.h"

#import "Time.h"
#import "Speaker.h"
#import "Location.h"
#import "Program.h"
#import "Venue.h"
#import "Information.h"
#import "Freepaper.h"
#import "Poster.h"
#import "FilmFestival.h"
#import "Album.h"
#import "Article.h"

#if DEBUG

#define IMPORT_DATA 0
#define IMPORT_FROM_LOCAL 1
#define DUMP_IMPORTED_DATA 0
#define UPDATE_SEED_DB 0

#endif // DEBUG


@implementation TTAppDelegate

+ (NSString *)SQLFilePath
{
    return [RKApplicationDataDirectory() stringByAppendingPathComponent:@"Data.sqlite"];
}

+ (RKManagedObjectStore *)defaultManagedObjectStore
{
    static RKManagedObjectStore *defaultManagedObjectStore;
    @synchronized(self) {
        if (!defaultManagedObjectStore) {
            NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
            defaultManagedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
            NSString *path = [self SQLFilePath];
#if IMPORT_DATA
            NSString *seedPath = nil;
            [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
#else
            NSString *seedPath = [[NSBundle mainBundle] pathForResource:@"Data" ofType:@"sqlite"];
#endif
            [defaultManagedObjectStore addSQLitePersistentStoreAtPath:path
                                               fromSeedDatabaseAtPath:seedPath
                                                    withConfiguration:nil
                                                              options:nil
                                                                error:nil];
            [defaultManagedObjectStore createManagedObjectContexts];
        }
    }
    return defaultManagedObjectStore;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
#ifndef DEBUG
#define TESTING 1
#ifdef TESTING
    [TestFlight setDeviceIdentifier:[[UIDevice currentDevice] uniqueIdentifier]];
#endif
    [TestFlight takeOff:@"9deb7929-e263-4602-bccd-43509c68dad5"];
#endif
    
    [application setStatusBarHidden:NO];
    
    if ([[UINavigationBar class] instancesRespondToSelector:@selector(setBarTintColor:)]) {
        [[UINavigationBar appearance] setBarTintColor:[TTAppTheme darkColor]];
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:[UIColor whiteColor]];
    } else {
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"NavBar"] forBarMetrics:UIBarMetricsDefault];
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:[TTAppTheme darkColor]];
        [[UILabel appearance] setBackgroundColor:[UIColor clearColor]];
    }
    
    UIImage *backButtonTransparentBackground = [UIImage imageWithColor:[UIColor clearColor] size:CGSizeMake(1, 35)];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonTransparentBackground
                                                      forState:UIControlStateNormal
                                                    barMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           UITextAttributeTextColor: [UIColor whiteColor],
                                                           UITextAttributeFont: [TTAppTheme mediumFontWithSize:18]
                                                           }];
    
    // Override point for customization after application launch.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
        UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
        splitViewController.delegate = (id)navigationController.topViewController;
    }
    
    RKManagedObjectStore *managedObjectStore = [TTAppDelegate defaultManagedObjectStore];
    [NSManagedObjectContext MR_initializeDefaultContextWithCoordinator:managedObjectStore.persistentStoreCoordinator];
    
#pragma mark - Map JSON to Coredata
    
    //
    // Speaker Mapping
    //
    RKEntityMapping *speakerMapping = [RKEntityMapping mappingForEntityForName:@"Speaker"
                                                          inManagedObjectStore:managedObjectStore];
    [speakerMapping addAttributeMappingsFromDictionary:
     @{
     @"speaker_id": @"id",
     @"speaker_name": @"name",
     @"speaker_image_url": @"imageURL",
     @"country": @"country"
     }];
    
    //
    // Location Mapping
    //
    RKEntityMapping *locationMapping = [RKEntityMapping mappingForEntityForName:@"Location"
                                                           inManagedObjectStore:managedObjectStore];
    [locationMapping addAttributeMappingsFromDictionary:
     @{
     @"location_id": @"id",
     @"location_name": @"name"
     }];
    
    //
    // Time Mapping
    //
    RKEntityMapping *timeMapping = [RKEntityMapping mappingForEntityForName:@"Time"
                                                       inManagedObjectStore:managedObjectStore];
    [timeMapping addAttributeMappingsFromDictionary:
     @{
     @"time_id": @"id",
     @"time_name": @"name"
     }];
    
    //
    // Topic Mapping
    //
    RKEntityMapping *topicMapping = [RKEntityMapping mappingForEntityForName:@"Topic"
                                                       inManagedObjectStore:managedObjectStore];
    [topicMapping addAttributeMappingsFromDictionary:
     @{
     @"topic_id": @"id",
     @"topic_name": @"name",
     @"speaker_order_array" : @"speakerOrder"
     }];
    [topicMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"speaker_array"
                                                                                 toKeyPath:@"speakers"
                                                                               withMapping:speakerMapping]];

    //
    // Program Mapping
    //
    RKEntityMapping *programMapping = [RKEntityMapping mappingForEntityForName:@"Program"
                                                        inManagedObjectStore:managedObjectStore];
    [programMapping addAttributeMappingsFromDictionary:
     @{
     @"program_id": @"id",
     @"program_title": @"title",
     @"program_type" : @"type",
     @"program_description" : @"desc",
     @"date" : @"date",
     @"keywords" : @"keywords",
     @"program_group_id" : @"programGroupId",
     @"program_group" : @"programGroup",
     @"is_program_highlighted" : @"isProgramHighlighted",
     @"highlight_order" : @"order",
     @"program_alt_name" : @"altName",     
     @"image" : @"image",
     @"time" : @"startTime",
     @"end_time" : @"endTime",
     @"is_deleted" : @"isHidden",
     @"speaker_order_array" : @"speakerOrder",
     @"topic_order_array" : @"topicOrder"
     }];
    
    [programMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"location"
                                                                                 toKeyPath:@"location"
                                                                               withMapping:locationMapping]];
    [programMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"speakers"
                                                                                 toKeyPath:@"speakers"
                                                                               withMapping:speakerMapping]];
    [programMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"topics"
                                                                                   toKeyPath:@"topics"
                                                                                 withMapping:topicMapping]];
    [programMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"time_slot"
                                                                                 toKeyPath:@"time"
                                                                               withMapping:timeMapping]];
    
    //
    // Venue Mapping
    //
    RKEntityMapping *venueMapping = [RKEntityMapping mappingForEntityForName:@"Venue"
                                                        inManagedObjectStore:managedObjectStore];
    [venueMapping addAttributeMappingsFromDictionary:
     @{
     @"id": @"id",
     @"exhibitor_name": @"exhibitorName",
     @"booth_no" : @"boothNo"
     }];
    
    //
    // Information Mapping
    //
    RKEntityMapping *informationMapping = [RKEntityMapping mappingForEntityForName:@"Information"
                                                        inManagedObjectStore:managedObjectStore];
    [informationMapping addAttributeMappingsFromDictionary:
     @{
     @"id": @"id",
     @"title": @"title",
     @"description" : @"desc",
     @"group" : @"group",
     @"contact" : @"contact"
     }];
    
    //
    // Free Paper Mapping
    //
    RKEntityMapping *freepaperMapping = [RKEntityMapping mappingForEntityForName:@"Freepaper"
                                                              inManagedObjectStore:managedObjectStore];
    [freepaperMapping addAttributeMappingsFromDictionary:
     @{
     @"free_paper_id": @"id",
     @"free_paper_title": @"title",
     @"date" : @"date",
     @"time" : @"startTime",
     @"end_time" : @"endTime",
     @"is_deleted" : @"isHidden"
     }];
    
    [freepaperMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"location"
                                                                                   toKeyPath:@"location"
                                                                                 withMapping:locationMapping]];
    [freepaperMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"speakers"
                                                                                   toKeyPath:@"speakers"
                                                                                 withMapping:speakerMapping]];
    [freepaperMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"topics"
                                                                                   toKeyPath:@"topics"
                                                                                 withMapping:topicMapping]];
    [freepaperMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"time_slot"
                                                                                   toKeyPath:@"time"
                                                                                 withMapping:timeMapping]];
    
    //
    // Poster Mapping
    //
    RKEntityMapping *posterMapping = [RKEntityMapping mappingForEntityForName:@"Poster"
                                                         inManagedObjectStore:managedObjectStore];
    [posterMapping addAttributeMappingsFromDictionary:
     @{
     @"id": @"id",
     @"title": @"title",
     @"synopsis": @"synopsis",
     @"number" : @"number",
     @"category" : @"category",
     @"category_id": @"categoryId"
     }];
    [posterMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"speaker"
                                                                                  toKeyPath:@"speaker"
                                                                                withMapping:speakerMapping]];

    
    //
    // FilmFestival Mapping
    //
    RKEntityMapping *filmFestivalMapping = [RKEntityMapping mappingForEntityForName:@"FilmFestival"
                                                         inManagedObjectStore:managedObjectStore];
    [filmFestivalMapping addAttributeMappingsFromDictionary:
     @{
     @"id": @"id",
     @"number": @"number",
     @"title": @"title",
     @"synopsis" : @"synopsis",
     @"category": @"category",
     @"category_id": @"categoryId",
     @"is_deleted": @"isHidden"
     }];
    [filmFestivalMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"speakers"
                                                                                        toKeyPath:@"speaker"
                                                                                      withMapping:speakerMapping]];
    
    //
    // Photo Mapping
    //
    RKEntityMapping *photoMapping = [RKEntityMapping mappingForEntityForName:@"Photo"
                                                        inManagedObjectStore:managedObjectStore];
    [photoMapping addAttributeMappingsFromDictionary:
     @{
     @"id": @"id",
     @"title": @"title",
     @"url": @"url",
     }];
    
    //
    // Album Mapping
    //
    RKEntityMapping *albumMapping = [RKEntityMapping mappingForEntityForName:@"Album"
                                                        inManagedObjectStore:managedObjectStore];
    [albumMapping addAttributeMappingsFromDictionary:
     @{
     @"id": @"id",
     @"title": @"title"
     }];
    
    [albumMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"photos"
                                                                                 toKeyPath:@"photos"
                                                                               withMapping:photoMapping]];
    
    //
    // Articles Mapping
    //
    RKEntityMapping *articleMapping = [RKEntityMapping mappingForEntityForName:@"Article"
                                                          inManagedObjectStore:managedObjectStore];
    [articleMapping addAttributeMappingsFromDictionary:
     @{
     @"id": @"id",
     @"title": @"title",
     @"url": @"url"
     }];

    // Set Date Format Matching Date Format from JSON
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [RKObjectMapping setPreferredDateFormatter:formatter];

#if IMPORT_DATA
    
    NSManagedObjectModel *model = managedObjectStore.managedObjectModel;
    for (NSEntityDescription *entity in model.entities) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity = entity;
        NSManagedObjectContext *context = [managedObjectStore persistentStoreManagedObjectContext];
        [context performBlockAndWait:^{
            NSError *error;
            NSArray *managedObjects = [context executeFetchRequest:fetchRequest error:&error];
            for (NSManagedObject *managedObject in managedObjects) {
                [context deleteObject:managedObject];
            }
        }];
    }
    
#endif
    
    NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful); // Anything in 2xx
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
#pragma mark - Import Programs from API
    //
    // Import Programs from API
    //
    {
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:programMapping
                                                                                                method:RKRequestMethodGET
                                                                                           pathPattern:nil
                                                                                               keyPath:@""
                                                                                           statusCodes:statusCodes];
#if IMPORT_FROM_LOCAL
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/apacrs/programs.json"]];
#else
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://apacrs.herokuapp.com/api/ios/programs"]];
#endif
        
        RKManagedObjectRequestOperation *operation = [[RKManagedObjectRequestOperation alloc] initWithRequest:request
                                                                                          responseDescriptors:@[responseDescriptor]];
        operation.managedObjectContext = managedObjectStore.mainQueueManagedObjectContext;
        operation.managedObjectCache = managedObjectStore.managedObjectCache;
        [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
            NIDPRINT(@"Completed importing programs");
            __block BOOL success;
            __block NSError *localError = nil;
            [managedObjectStore.mainQueueManagedObjectContext performBlockAndWait:^{
                success = [managedObjectStore.mainQueueManagedObjectContext save:&localError];
                if (! success) {
                    RKLogCoreDataError(localError);
                }
            }];
            
#if DUMP_IMPORTED_DATA
            NSArray *programs = [Program MR_findAll];
            NSAssert(programs.count, @"No Programs Found");
            for (Program *program in programs) {
                NSLog(@"%@", @"=== Title ===");
                NSLog(@"%@", program.title);
                NSLog(@"%@", program.date);
                NSLog(@"%@", @"=== Speakers ===");
                for (Speaker *speaker in program.speakers) {
                    NSLog(@"%@", speaker.name);
                }
                NSLog(@"%@", @"=== Location ===");
                NSLog(@"%@", program.location.name);
                NSLog(@"%@", @"=== ==== ===");
            }
#endif
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NIDPRINT(@"Error importing programs: %@", error);
        }];
        
        [queue addOperation:operation];
    }
    
#pragma mark - Import Venue from API
    //
    // Import Venue from API
    //
    {
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:venueMapping
                                                                                                method:RKRequestMethodGET
                                                                                           pathPattern:nil
                                                                                               keyPath:@""
                                                                                           statusCodes:statusCodes];
#if IMPORT_FROM_LOCAL
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/venues.json"]];
#else
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://apacrs.herokuapp.com/api/ios/venues"]];
#endif
        
        RKManagedObjectRequestOperation *operation = [[RKManagedObjectRequestOperation alloc] initWithRequest:request
                                                                                          responseDescriptors:@[responseDescriptor]];
        operation.managedObjectContext = managedObjectStore.mainQueueManagedObjectContext;
        operation.managedObjectCache = managedObjectStore.managedObjectCache;
        [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
            NIDPRINT(@"Completed importing venues");
            __block BOOL success;
            __block NSError *localError = nil;
            [managedObjectStore.mainQueueManagedObjectContext performBlockAndWait:^{
                success = [managedObjectStore.mainQueueManagedObjectContext save:&localError];
                if (! success) {
                    RKLogCoreDataError(localError);
                }
            }];
            
#if DUMP_IMPORTED_DATA
            NSArray *venues = [Venue MR_findAll];
            NSAssert(venues.count, @"No Venue Found");
            for (Venue *venue in venues) {
                NSLog(@"%@", @"=== Booth No, Exhibitor Name ===");
                NSLog(@"%@, %@", venue.boothNo, venue.exhibitorName);
                NSLog(@"%@", @"=== ==== ===");
            }
#endif
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NIDPRINT(@"Error importing venues: %@", error);
        }];
        
        [queue addOperation:operation];
    }
    
#pragma mark - Import Information from API
    //
    // Import Information from API
    //
    {
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:informationMapping
                                                                                                method:RKRequestMethodGET
                                                                                           pathPattern:nil
                                                                                               keyPath:@""
                                                                                           statusCodes:statusCodes];
#if IMPORT_FROM_LOCAL
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/information.json"]];
#else
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://apacrs.herokuapp.com/api/ios/information"]];
#endif
        
        RKManagedObjectRequestOperation *operation = [[RKManagedObjectRequestOperation alloc] initWithRequest:request
                                                                                          responseDescriptors:@[responseDescriptor]];
        operation.managedObjectContext = managedObjectStore.mainQueueManagedObjectContext;
        operation.managedObjectCache = managedObjectStore.managedObjectCache;
        [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
            NIDPRINT(@"Completed importing information");
            __block BOOL success;
            __block NSError *localError = nil;
            [managedObjectStore.mainQueueManagedObjectContext performBlockAndWait:^{
                success = [managedObjectStore.mainQueueManagedObjectContext save:&localError];
                if (! success) {
                    RKLogCoreDataError(localError);
                }
            }];
            
#if DUMP_IMPORTED_DATA
            NSArray *items = [Information MR_findAll];
            NSAssert(items.count, @"No Information Found");
            for (Information *item in items) {
                NSLog(@"%@", @"=== title, contact ===");
                NSLog(@"%@, %@", item.title, item.contact);
                NSLog(@"%@", @"=== ==== ===");
            }
#endif
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NIDPRINT(@"Error importing information: %@", error);
        }];
        
        [queue addOperation:operation];
    }
    
#pragma mark - Import Frepaper from API
    //
    // Import Frepaper from API
    //
    {
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:freepaperMapping
                                                                                                method:RKRequestMethodGET
                                                                                           pathPattern:nil
                                                                                               keyPath:@""
                                                                                           statusCodes:statusCodes];
#if IMPORT_FROM_LOCAL
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/apacrs/free_papers.json"]];
#else
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://apacrs.herokuapp.com/api/ios/free_papers"]];
#endif
        
        RKManagedObjectRequestOperation *operation = [[RKManagedObjectRequestOperation alloc] initWithRequest:request
                                                                                          responseDescriptors:@[responseDescriptor]];
        operation.managedObjectContext = managedObjectStore.mainQueueManagedObjectContext;
        operation.managedObjectCache = managedObjectStore.managedObjectCache;
        [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
            NIDPRINT(@"Completed importing Freepaper");
            __block BOOL success;
            __block NSError *localError = nil;
            [managedObjectStore.mainQueueManagedObjectContext performBlockAndWait:^{
                success = [managedObjectStore.mainQueueManagedObjectContext save:&localError];
                if (! success) {
                    RKLogCoreDataError(localError);
                }
            }];
            
#if DUMP_IMPORTED_DATA
            NSArray *items = [Freepaper MR_findAll];
            NSAssert(items.count, @"No Freepaper Found");
            for (Freepaper *item in items) {
                NSLog(@"%@", @"=== title, date ===");
                NSLog(@"%@, %@", item.title, item.date);
                NSLog(@"%@", @"=== ==== ===");
            }
#endif
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NIDPRINT(@"Error importing Freepaper: %@", error);
        }];
        
        [queue addOperation:operation];
    }
    
#pragma mark - Import Poster from API
    //
    // Import Poster from API
    //
    {
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:posterMapping
                                                                                                method:RKRequestMethodGET
                                                                                           pathPattern:nil
                                                                                               keyPath:@""
                                                                                           statusCodes:statusCodes];
#if IMPORT_FROM_LOCAL
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/posters.json"]];
#else
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://apacrs.herokuapp.com/api/ios/posters"]];
#endif
        
        RKManagedObjectRequestOperation *operation = [[RKManagedObjectRequestOperation alloc] initWithRequest:request
                                                                                          responseDescriptors:@[responseDescriptor]];
        operation.managedObjectContext = managedObjectStore.mainQueueManagedObjectContext;
        operation.managedObjectCache = managedObjectStore.managedObjectCache;
        [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
            NIDPRINT(@"Completed importing Poster");
            __block BOOL success;
            __block NSError *localError = nil;
            [managedObjectStore.mainQueueManagedObjectContext performBlockAndWait:^{
                success = [managedObjectStore.mainQueueManagedObjectContext save:&localError];
                if (! success) {
                    RKLogCoreDataError(localError);
                }
            }];
            
#if DUMP_IMPORTED_DATA
            NSArray *items = [Poster MR_findAll];
            NSAssert(items.count, @"No Poster Found");
            for (Poster *item in items) {
                NSLog(@"%@", @"=== title, number ===");
                NSLog(@"%@, %@", item.title, item.number);
                NSLog(@"%@", @"=== ==== ===");
            }
#endif
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NIDPRINT(@"Error importing Poster: %@", error);
        }];
        
        [queue addOperation:operation];
    }

#pragma mark - Import FilmFestival from API
    //
    // Import FilmFestival from API
    //
    {
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:filmFestivalMapping
                                                                                                method:RKRequestMethodGET
                                                                                           pathPattern:nil
                                                                                               keyPath:@""
                                                                                           statusCodes:statusCodes];
#if IMPORT_FROM_LOCAL
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/apacrs/film_festivals.json"]];
#else
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://apacrs.herokuapp.com/api/ios/film_festivals"]];
#endif
        
        RKManagedObjectRequestOperation *operation = [[RKManagedObjectRequestOperation alloc] initWithRequest:request
                                                                                          responseDescriptors:@[responseDescriptor]];
        operation.managedObjectContext = managedObjectStore.mainQueueManagedObjectContext;
        operation.managedObjectCache = managedObjectStore.managedObjectCache;
        [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
            NIDPRINT(@"Completed importing FilmFestival");
            __block BOOL success;
            __block NSError *localError = nil;
            [managedObjectStore.mainQueueManagedObjectContext performBlockAndWait:^{
                success = [managedObjectStore.mainQueueManagedObjectContext save:&localError];
                if (! success) {
                    RKLogCoreDataError(localError);
                }
            }];
            
#if DUMP_IMPORTED_DATA
            NSArray *items = [FilmFestival MR_findAll];
            NSAssert(items.count, @"No FilmFestival Found");
            for (FilmFestival *item in items) {
                NSLog(@"%@", @"=== title, synopsis ===");
                NSLog(@"%@, %@", item.title, item.synopsis);
                NSLog(@"%@", @"=== ==== ===");
            }
#endif
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NIDPRINT(@"Error importing FilmFestival: %@", error);
        }];
        
        [queue addOperation:operation];
    }
    
#pragma mark - Import Albums from API
    //
    // Import Albums from API
    //
    {
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:albumMapping
                                                                                                method:RKRequestMethodGET
                                                                                           pathPattern:nil
                                                                                               keyPath:@""
                                                                                           statusCodes:statusCodes];
#if IMPORT_FROM_LOCAL
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/albums.json"]];
#else
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://apacrs.herokuapp.com/api/ios/albums"]];
#endif
        
        RKManagedObjectRequestOperation *operation = [[RKManagedObjectRequestOperation alloc] initWithRequest:request
                                                                                          responseDescriptors:@[responseDescriptor]];
        operation.managedObjectContext = managedObjectStore.mainQueueManagedObjectContext;
        operation.managedObjectCache = managedObjectStore.managedObjectCache;
        [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
            NIDPRINT(@"Completed importing Albums");
            __block BOOL success;
            __block NSError *localError = nil;
            [managedObjectStore.mainQueueManagedObjectContext performBlockAndWait:^{
                success = [managedObjectStore.mainQueueManagedObjectContext save:&localError];
                if (! success) {
                    RKLogCoreDataError(localError);
                }
            }];
            

            NSArray *items = [Album MR_findAll];
            NSAssert(items.count, @"No Albums Found");
            for (Album *item in items) {
                NSLog(@"%@", @"=== title, photos ===");
                NSLog(@"%@, %@", item.title, item.photos);
                NSLog(@"%@", @"=== ==== ===");
            }

            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NIDPRINT(@"Error importing FilmFestival: %@", error);
        }];
        
        [queue addOperation:operation];
    }
    
#pragma mark - Import Article from API
    //
    // Import Article from API
    //
    {
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:articleMapping
                                                                                                method:RKRequestMethodGET
                                                                                           pathPattern:nil
                                                                                               keyPath:@""
                                                                                           statusCodes:statusCodes];
#if IMPORT_FROM_LOCAL
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://localhost:8888/articles.json"]];
#else
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://apacrs.herokuapp.com/api/ios/articles"]];
#endif
        
        RKManagedObjectRequestOperation *operation = [[RKManagedObjectRequestOperation alloc] initWithRequest:request
                                                                                          responseDescriptors:@[responseDescriptor]];
        operation.managedObjectContext = managedObjectStore.mainQueueManagedObjectContext;
        operation.managedObjectCache = managedObjectStore.managedObjectCache;
        [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
            NIDPRINT(@"Completed importing Articles");
            __block BOOL success;
            __block NSError *localError = nil;
            [managedObjectStore.mainQueueManagedObjectContext performBlockAndWait:^{
                success = [managedObjectStore.mainQueueManagedObjectContext save:&localError];
                if (! success) {
                    RKLogCoreDataError(localError);
                }
            }];
            

            NSArray *items = [Article MR_findAll];
            NSAssert(items.count, @"No Articles Found");
            for (Article *item in items) {
                NSLog(@"%@", @"=== title, url ===");
                NSLog(@"%@, %@", item.title, item.url);
                NSLog(@"%@", @"=== ==== ===");
            }

            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            NIDPRINT(@"Error importing Articles: %@", error);
        }];
        
        [queue addOperation:operation];
    }
    
#if UPDATE_SEED_DB
    dispatch_async(dispatch_queue_create(NULL, DISPATCH_QUEUE_CONCURRENT), ^{
        NSError *error;
        [queue waitUntilAllOperationsAreFinished];
        NSString *seedSQL = [NSString stringWithFormat:@"%s/Data.sqlite", dirname(__FILE__)];
        NSFileManager *manager = [NSFileManager defaultManager];
        if ([manager fileExistsAtPath:seedSQL]) {
            [manager removeItemAtPath:seedSQL error:&error];
        }
        [manager copyItemAtPath:[TTAppDelegate SQLFilePath] toPath:seedSQL error:&error];
        if (error == nil) {
            NSLog(@"SeedDB updated");
        } else {
            NSLog(@"Error updating SeedDB: %@", error);
        }
    });
#endif // UPDATE_SEED_DB

    return YES;
}

- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"APACRS"
                                                        message:notification.alertBody
                                                       delegate:self cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
