//
//  TTPosterDetailViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/29/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTPosterDetailViewController.h"
#import "Poster.h"
#import "Speaker.h"

@interface TTPosterDetailViewController ()

@end

@implementation TTPosterDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.poster) {
        [self loadHTML];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [super webViewDidFinishLoad:webView];
    self.title = self.poster.title;
}

- (void)setPoster:(Poster *)poster
{
    if (poster != _poster) {
        _poster = poster;
        [self loadHTML];
    }
}

- (NSDictionary *)replaceValues
{
    NSMutableDictionary *values;
    values = [@{
              @"Program-Title": self.poster.title ? self.poster.title : @"",
              @"Program-Description": self.poster.synopsis ? self.poster.synopsis : @"",
              @"Program-Speaker": self.poster.speaker ? self.poster.speaker.name : @"",
              } mutableCopy];
    return values;
}

@end
