//
//  TTFreepaperDetailViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTFreepaperDetailViewController.h"
#import "Freepaper.h"
#import "Time.h"
#import "Location.h"
#import "Speaker.h"
#import "Speaker+Additions.h"
#import "Topic.h"

@interface TTFreepaperDetailViewController ()

@end

@implementation TTFreepaperDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    if (self.freepaper) {
        [self loadHTML];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

#pragma makr - WebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.title = self.freepaper.title;
    [super webViewDidFinishLoad:webView];
}

#pragma mark -
#pragma mark Freepaper

- (void)setFreepaper:(Freepaper *)freepaper
{
    if (_freepaper != freepaper) {
        _freepaper = freepaper;
        [self loadHTML];
    }
}

- (NSDictionary *)replaceValues
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMM"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSString *speakerFormat = @"<span>%@</span>";
    NSString *speakerList = [[self.freepaper.speakers.allObjects speakerNamesWithFormatString:speakerFormat] componentsJoinedByString:@"\n"];
    
    NSMutableArray *topicAndSpeakerList = [NSMutableArray new];
    
    NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    NSArray *sorted = [self.freepaper.topics sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];
    
    for (Topic *topic in sorted) {
        NSString *topicAndSpeaker = [NSString stringWithFormat:@"<li><h5 class=\"title\">%@</h5><span>%@</span></li>", topic.name, [[topic.speakers anyObject] name]];
        [topicAndSpeakerList addObject:topicAndSpeaker];
    }
    
    NSString *topicAndSpeaker = [topicAndSpeakerList componentsJoinedByString:@"\n"];
    
    
    NSMutableDictionary *variables = [@{
                                      @"Program-Title": self.freepaper.title ? self.freepaper.title : @"",
                                      @"Program-Date": self.freepaper.date ? [formatter stringFromDate:self.freepaper.date] : @"",
                                      @"Program-Time": [NSString stringWithFormat:@"%@ - %@hrs", self.freepaper.startTime, self.freepaper.endTime],
                                      @"Program-Speakers": speakerList.length ? speakerList : @"<span>N/A</span>",
                                      @"Topics-Speakers": topicAndSpeaker.length ? topicAndSpeaker : @"<li><span>N/A</span></li>"
                                      } mutableCopy];
    
    if (self.freepaper.location.name) {
        variables[@"Program-Location"] = self.freepaper.location.name;
    } else {
        variables[@"Program-Location"] = @"N/A";
    }
    
    return variables;
}

@end
