//
//  TTMasterViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/12/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TTDetailViewController;

#import <CoreData/CoreData.h>

@interface TTMasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) TTDetailViewController *detailViewController;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
