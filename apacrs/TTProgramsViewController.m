//
//  TTProgramsViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/12/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTProgramsViewController.h"
#import "SVSegmentedControl.h"
#import "TTProgramDetailViewController.h"
#import "TTProgramCell.h"
#import "NSString+DateFormatAddition.h"
#import "NSArray+TTAddition.h"
#import "UIImage+Addition.h"

#import "Program.h"
#import "Program+Additions.h"
#import "Time.h"
#import "Time+Additions.h"

#import "TTAppTheme.h"

@interface TTProgramsViewController ()

@end

@implementation TTProgramsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.cellFactory mapObjectClass:[Program class] toCellClass:[TTProgramCell class]];
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIScrollView *dateScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 43)];
    UIScrollView *timeScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 43, self.view.width, 25)];
    
    dateScrollView.backgroundColor = [UIColor blackColor];
    timeScrollView.backgroundColor = [UIColor blackColor];
    
    dateScrollView.showsHorizontalScrollIndicator = NO;
    timeScrollView.showsHorizontalScrollIndicator = NO;
    
    _dateChooser = [[SVSegmentedControl alloc] initWithSectionTitles:[self dateTitles]];
    [_dateChooser setSelectedSegmentIndex:0 animated:NO];
    _dateChooser.textColor = [UIColor blackColor];
    _dateChooser.textShadowOffset = CGSizeMake(0, 0);
    _dateChooser.font = [TTAppTheme regularFontWithSize:14];

    _timeChooser = [[SVSegmentedControl alloc] initWithSectionTitles:[self timeTitles]];
    _timeChooser.textColor = [UIColor colorWithRed:0.596 green:0.804 blue:0.855 alpha:1.000];
    _timeChooser.textShadowOffset = CGSizeMake(0, 0);
    _timeChooser.font = [UIFont boldSystemFontOfSize:14];
    
    UIImage *transparentBackground = [[UIImage imageWithColor:[UIColor colorWithWhite:1.0 alpha:0.0] size:CGSizeMake(1.0, 30)]
                                      resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    UIImage *darkThemeBackground = [UIImage imageWithColor:[TTAppTheme darkColor] size:CGSizeMake(1.0, 30)];
    
    dateScrollView.backgroundColor = [TTAppTheme backgroundColor];
    _dateChooser.backgroundImage = transparentBackground;
    _dateChooser.thumb.backgroundImage = darkThemeBackground;
    _dateChooser.thumb.highlightedBackgroundImage = darkThemeBackground;
    _dateChooser.thumb.textShadowOffset = CGSizeMake(0, 0);
    _dateChooser.thumbEdgeInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    UIImage *timeThumbBackground = [UIImage imageWithColor:[UIColor colorWithWhite:1.000 alpha:0.15] size:CGSizeMake(1, _timeChooser.height)];
    timeScrollView.backgroundColor = [UIColor colorWithRed:0.204 green:0.608 blue:0.710 alpha:1.000];
    _timeChooser.backgroundImage = transparentBackground;
    _timeChooser.thumb.backgroundImage = timeThumbBackground;
    _timeChooser.thumb.highlightedBackgroundImage = timeThumbBackground;
    _timeChooser.thumb.textShadowOffset = CGSizeMake(0, 0);
    _timeChooser.thumbEdgeInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    // layout
    
    [dateScrollView addSubview:_dateChooser];
    [timeScrollView addSubview:_timeChooser];
    
    _dateChooser.origin = CGPointMake(0, 0);
    _timeChooser.origin = CGPointMake(0, 0);
    
    _dateChooser.center = CGPointMake(_dateChooser.center.x, dateScrollView.height / 2);
    _timeChooser.center = CGPointMake(_timeChooser.center.x, timeScrollView.height / 2);
    
    dateScrollView.contentSize = _dateChooser.size;
    timeScrollView.contentSize = _timeChooser.size;
    
    [_dateChooser addTarget:self action:@selector(dateDidChange:) forControlEvents:UIControlEventValueChanged];
    [_timeChooser addTarget:self action:@selector(timeDidChange:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:dateScrollView];
    [self.view addSubview:timeScrollView];
    
    _dateScrollView = dateScrollView;
    _timeScrollView = timeScrollView;
    
    self.tableView.frame = CGRectMake(0,
                                      CGRectGetMaxY(timeScrollView.frame),
                                      self.view.width,
                                      self.view.height - CGRectGetMaxY(timeScrollView.frame));
}

- (NSArray *)dateTitles
{
    NSArray *titles = [Program dateTitles];
    NSAssert(titles.count, @"No Date Titles");
    return titles;
}

- (NSArray *)timeTitles
{
    NSString *dateTitle = _dateChooser.sectionTitles[_dateChooser.selectedSegmentIndex];
    NSAssert(dateTitle, @"No Date Title is selected");
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %@",
                              @"date", [dateTitle dateValue]];

    NSArray *programs = [Program MR_findAllWithPredicate:predicate];
    NSAssert(programs.count, @"No progams found for %@", dateTitle);
    NSArray *times = [programs arrayOfKeyPath:@"time"];
    NSAssert(times.count, @"No times are extracted from programs");
    NSMutableArray *titles = [[times timeTitles] mutableCopy];
    NSAssert(titles.count, @"No Time Titles");
    [titles insertObject:@"All" atIndex:0];
    return titles;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self reloadEvents];

    self.title = NSLocalizedString(@"Program by Day", nil);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dateDidChange:(id)sender
{
    _timeChooser.sectionTitles = [self timeTitles];
    UIScrollView *scroll = (UIScrollView *)_timeChooser.superview;
    [_timeChooser sizeToFit];
    _timeChooser.origin = CGPointMake(0, 0);
    _timeChooser.center = CGPointMake(_timeChooser.center.x, self.timeScrollView.height / 2);
    scroll.contentSize = [_timeChooser size];
    [self reloadEvents];
}

- (void)timeDidChange:(id)sender
{
    [self reloadEvents];
}

- (NSArray *)programsForSelectedDateAndTime
{
    NSString *dayTitle = _dateChooser.sectionTitles[_dateChooser.selectedSegmentIndex];
    NSString *timeTitle = _timeChooser.sectionTitles[_timeChooser.selectedSegmentIndex];
    NSPredicate *predicate;
    if ([timeTitle isEqualToString:@"All"]) {
        predicate = [NSPredicate predicateWithFormat:@"%K = %@ AND %K = %@",
                     @"date", [dayTitle dateValue],
                     @"isHidden", [NSNumber numberWithBool:NO]];
    } else {
        predicate = [NSPredicate predicateWithFormat:@"%K = %@ AND %K = %@ AND %K = %@",
                     @"date", [dayTitle dateValue],
                     @"time.name", timeTitle,
                     @"isHidden", [NSNumber numberWithBool:NO]];
    }
    NSArray *programs = [Program MR_findAllWithPredicate:predicate];
    return programs;
}

- (void)reloadEvents
{
    NSArray *list = [self programsForSelectedDateAndTime];
    list = [list sortedArrayUsingComparator:^NSComparisonResult(Program *p1, Program *p2) {
        return [p1.startTime compare:p2.startTime options:NSNumericSearch];
    }];
    self.tableModel = [[NITableViewModel alloc] initWithListArray:list
                                                         delegate:self.cellFactory];
    
    [self setEmptyView:nil];
    if (list.count == 0) {
        UILabel *label = [[UILabel alloc] initWithFrame:self.tableView.frame];
        label.text = [self emptyText];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [TTAppTheme regularFontWithSize:18];
        label.textColor = [UIColor colorWithWhite:0 alpha:0.7];
        label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        label.backgroundColor = [TTAppTheme backgroundColor];
        [self setEmptyView:label];
    }
}

- (NSString *)emptyText
{
    return NSLocalizedString(@"No Programs Available", nil);
}

- (CGRect)emptyViewFrame
{
    return self.tableView.frame;
}

- (void)setEmptyView:(UIView *)emptyView
{
    if (_emptyView != emptyView) {
        [_emptyView removeFromSuperview];
        _emptyView = emptyView;
    }
    
    if (_emptyView != nil) {
        emptyView.frame = [self emptyViewFrame];
        [self.view addSubview:emptyView];
    }
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITableViewModel *model = (NITableViewModel *)tableView.dataSource;
    TTProgramDetailViewController *programDetailViewController = [[TTProgramDetailViewController alloc] initWithNibName:nil bundle:nil];
    programDetailViewController.program = [model objectAtIndexPath:indexPath];
    [self.navigationController pushViewController:programDetailViewController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.cellFactory tableView:tableView heightForRowAtIndexPath:indexPath model:tableView.dataSource];
}

@end
