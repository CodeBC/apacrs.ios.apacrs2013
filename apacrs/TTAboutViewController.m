//
//  TTAboutViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/4/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTAboutViewController.h"
#import "TTAppTheme.h"

@interface TTAboutViewController ()

@end

@implementation TTAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    NSURL *htmlURL = [[NSBundle mainBundle] URLForResource:@"AboutHTML/about" withExtension:@"html"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:htmlURL];
    
    [self.webView loadRequest:request];
    
    self.webView.backgroundColor = [TTAppTheme backgroundColor];
    
    self.title = NSLocalizedString(@"About", nil);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
