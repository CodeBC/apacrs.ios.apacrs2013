//
//  TTInformationCell.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTTitleCell.h"
#import "Information.h"

@interface TTInformationCell : TTTitleCell

@property (nonatomic, strong) Information *item;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UILabel *contactLabel;

@end
