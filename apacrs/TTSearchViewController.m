//
//  TTSearchViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/16/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "TTSearchViewController.h"
#import "TTButtonCellObject.h"
#import "TTPickerCellObject.h"
#import "TTSearchResultViewController.h"

#import "Program.h"
#import "Program+Additions.h"
#import "Topic.h"

#import "TTAppTheme.h"

#define OVERLAY_VIEW_TAG 0x8888
#define INFO_LABEL_TAG 0x9999

@interface TTSearchViewController () <UITableViewDelegate>

@property (nonatomic, assign) BOOL ignoreKeyboardToggle;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NITableViewModel *model;
@property (nonatomic, strong) NICellFactory *cellFactory;
@property (nonatomic, strong) TTSearchResultViewController *searchResultController;

@end

@implementation TTSearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillToggleVisibility:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillToggleVisibility:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
    }
    
    return self;
}

- (void)keyboardWillToggleVisibility:(NSNotification *)notification
{
    if (self.ignoreKeyboardToggle) {
        return;
    }

    CGRect endFrame = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    endFrame = [self.view convertRect:endFrame fromView:nil];
    double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    NSUInteger animationCurve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationCurve:animationCurve];
    [UIView setAnimationDuration:duration];
    
    self.tableView.frame = CGRectMake(0, 0, self.view.width, endFrame.origin.y);
    
    [UIView commitAnimations];
}

- (UITableViewStyle)tableViewStyle
{
    return UITableViewStyleGrouped;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Search", nil);
    
    
    NSArray *list = @[
                      NSLocalizedString(@"Search By", nil),
                      [NITextInputFormElement textInputElementWithID:0 placeholderText:@"Program/Keyword/Speaker" value:nil],
                      @"",
                      [TTButtonCellObject objectWithTitle:NSLocalizedString(@"Search", nil)]
                      ];
    self.tableModel = [[NITableViewModel alloc] initWithSectionedArray:list delegate:self.cellFactory];
    
    self.tableView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.tableView.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    self.tableView.layer.shadowOpacity = 0.5;
    self.tableView.layer.shadowRadius = 3.0;
    self.tableView.layer.masksToBounds = NO;
    [self.view addSubview:self.tableView];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 0) {
        [self showSearchResultsView];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UIImageView *)snapViewOfTableView
{
    UIImageView *tableViewSnapView = NISnapshotViewOfView(self.tableView);
    tableViewSnapView.layer.shadowColor = self.tableView.layer.shadowColor;
    tableViewSnapView.layer.shadowRadius = self.tableView.layer.shadowRadius;
    tableViewSnapView.layer.shadowOpacity = self.tableView.layer.shadowOpacity;
    tableViewSnapView.layer.shadowOffset = self.tableView.layer.shadowOffset;
    tableViewSnapView.layer.masksToBounds = self.tableView.layer.masksToBounds;
    tableViewSnapView.layer.shadowPath = self.tableView.layer.shadowPath;
    
    return tableViewSnapView;
}

- (void)showSearchResultsView
{
    CGFloat peek = (self.view.height/7.0)*6.0;
    CGFloat bounceBack = 10.0f;
    
    self.ignoreKeyboardToggle = YES;
    [self.view endEditing:YES];
    self.ignoreKeyboardToggle = NO;
    
    UIView *overlayView = [[UIView alloc] initWithFrame:self.tableView.frame];
    overlayView.tag = OVERLAY_VIEW_TAG;
    overlayView.backgroundColor = [TTAppTheme darkColor];
    overlayView.alpha = 0.0f;
    
    UILabel *tapInfoLabel = [[UILabel alloc] init];
    tapInfoLabel.tag = INFO_LABEL_TAG;
    tapInfoLabel.text = NSLocalizedString(@"Tap here for new search", nil);
    tapInfoLabel.font = [TTAppTheme mediumFontWithSize:15];
    tapInfoLabel.alpha = 0.0f;
    tapInfoLabel.backgroundColor = [UIColor clearColor];
    tapInfoLabel.textColor = [TTAppTheme darkColor];
    [tapInfoLabel sizeToFit];
    tapInfoLabel.frame = CGRectMake(self.view.center.x - tapInfoLabel.width / 2, 0, tapInfoLabel.width, tapInfoLabel.height);
    tapInfoLabel.center = CGPointMake(tapInfoLabel.center.x, (self.view.height - peek) / 2);
    [self.view addSubview:tapInfoLabel];
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(showSearchForm)];
    [overlayView addGestureRecognizer:gestureRecognizer];
    [self.view insertSubview:overlayView aboveSubview:self.tableView];
    
    self.searchResultController = [[TTSearchResultViewController alloc] init];
    self.searchResultController.programs = [self searchPrograms];
    
    // while bouncing, the background is visible, so make it looks good by matching colors
    self.view.backgroundColor = [TTAppTheme backgroundColor];
    
    [self.searchResultController willMoveToParentViewController:self];
    [self addChildViewController:self.searchResultController];
    [self.view insertSubview:self.searchResultController.view atIndex:0];
    self.searchResultController.view.frame = CGRectMake(0, self.view.height-peek, self.view.width, peek);
    [self.searchResultController didMoveToParentViewController:self];
    
    self.tableView.userInteractionEnabled = NO;
    self.tableView.layer.shadowPath = CGPathCreateWithRect(CGRectInset(self.tableView.bounds, -5, -5), &CGAffineTransformIdentity);
    
    UIViewAnimationOptions options = UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut;
    [UIView animateWithDuration:0.3 delay:0 options:options animations:^{
        self.tableView.frame = CGRectMake(0, -peek-bounceBack, self.view.width, self.view.height);
        overlayView.frame = CGRectMake(0, -peek-bounceBack, self.view.width, self.view.height);
        overlayView.alpha = 0.25f;
        tapInfoLabel.alpha = 1.0f;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.15 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.tableView.frame = CGRectMake(0, -peek, self.view.width, self.view.height);
            overlayView.frame = CGRectMake(0, -peek, self.view.width, self.view.height);
        } completion:^(BOOL finished) {
            self.tableView.userInteractionEnabled = YES;
            self.tableView.layer.shadowPath = CGPathCreateWithRect(CGRectInset(self.tableView.bounds, -5, -5), &CGAffineTransformIdentity);
        }];
    }];
}

- (void)showSearchForm
{
    UIView *overlayView = [self.view viewWithTag:OVERLAY_VIEW_TAG];
    UILabel *tapInfoLabel = [self.view viewWithTag:INFO_LABEL_TAG];
    
    CGFloat bounceBack = 10.0f;
    
    self.tableView.userInteractionEnabled = NO;
    
    UIViewAnimationOptions options = UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut;
    [UIView animateWithDuration:0.3 delay:0 options:options animations:^{
        self.tableView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
        overlayView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
        overlayView.alpha = 0.0f;
        tapInfoLabel.alpha = 0.0f;
    } completion:^(BOOL finished) {
        [overlayView removeFromSuperview];
        [tapInfoLabel removeFromSuperview];
        [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.tableView.frame = CGRectMake(0, -bounceBack, self.view.width, self.view.height);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.133 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.tableView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
            } completion:^(BOOL finished) {
                [self.searchResultController willMoveToParentViewController:nil];
                [self.searchResultController.view removeFromSuperview];
                [self.searchResultController removeFromParentViewController];
                self.searchResultController = nil;
                
                self.tableView.userInteractionEnabled = YES;
            }];
        }];
    }];
}

- (NSArray *)searchPrograms
{
    NITableViewModel *model = (NITableViewModel *)self.tableView.dataSource;
    NITextInputFormElement *programElement = [model objectAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
  
    NSString *searchString = [programElement value];
    
    NSMutableArray *predicateFormat = [NSMutableArray new];
    NSMutableArray *predicateArgs = [NSMutableArray new];
    
    if (searchString.length) {
        [predicateFormat addObject:
         @"ANY %K contains[cd] %@ OR ANY %K contains[cd] %@ OR ANY %K contains[cd] %@"
         @" OR "
         @"ANY %K contains[cd] %@ OR ANY %K contains[cd] %@ OR ANY %K IN %@"
         @" OR "
         @"%K contains[cd] %@"];
        [predicateArgs addObject:@"title"];
        [predicateArgs addObject:searchString];
        [predicateArgs addObject:@"keywords"];
        [predicateArgs addObject:searchString];
        [predicateArgs addObject:@"desc"];
        [predicateArgs addObject:searchString];

        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY %K contains[cd] %@ OR ANY %K contains[cd] %@",
                                  @"speakers.name", searchString,
                                  @"speakers.country", searchString];
        
        NSArray *topics = [Topic MR_findAllWithPredicate:predicate];
        
        [predicateArgs addObject:@"speakers.name"];
        [predicateArgs addObject:searchString];
        [predicateArgs addObject:@"speakers.country"];
        [predicateArgs addObject:searchString];
        [predicateArgs addObject:@"topics"];
        [predicateArgs addObject:topics];
        
        [predicateArgs addObject:@"keywords"];
        [predicateArgs addObject:searchString];
    }
    

    if (predicateFormat.count == 0) {
        return nil;
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[predicateFormat componentsJoinedByString:@" AND "]
                                                argumentArray:predicateArgs];
    NSArray *programs = [Program MR_findAllWithPredicate:predicate];
    
    return programs;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    if(section == 1) {
    
        UIView * childView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, 320, 240)];
        
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 20, 240, 80)];
        textLabel.backgroundColor = [UIColor clearColor];
        textLabel.font = [TTAppTheme regularFontWithSize:15];
        textLabel.shadowColor = [UIColor colorWithWhite:1.0 alpha:0.8];
        textLabel.numberOfLines = 5;
        textLabel.textAlignment = NSTextAlignmentCenter;
        
        textLabel.text = @"Search function excludes content from the Film Festival, Posters and Free Papers.";
        [childView addSubview:textLabel];
        
        return childView;
    }
    return nil;
}


@end
