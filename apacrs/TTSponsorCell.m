//
//  TTSponsorCell.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/9/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTSponsorCell.h"
#import "TTSponsor.h"
#import "TTSponsorCellObject.h"

@implementation TTSponsorCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.backgroundView = self.backgroundImageView;
        CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
        CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
        CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
        UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
        self.backgroundView.backgroundColor = color;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    TTSponsorCellObject *sponsorObject = (TTSponsorCellObject *)object;
    if ([sponsorObject isKindOfClass:[TTSponsorCellObject class]]) {
        TTSponsor *sponsor = sponsorObject.sponsor;
        self.backgroundImageView.image = sponsor.bannerImage;
    }
    
    return YES;
}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    TTSponsorCellObject *sponsorObject = (TTSponsorCellObject *)object;
    if ([sponsorObject isKindOfClass:[TTSponsorCellObject class]]) {
        TTSponsor *sponsor = sponsorObject.sponsor;
        if (sponsor.bannerImage) {
            return sponsor.bannerImage.size.height / 2;
        }
    }
    return tableView.rowHeight;
}

@end
