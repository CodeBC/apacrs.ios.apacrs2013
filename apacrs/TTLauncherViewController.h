//
//  TTLauncherViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/12/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTLauncherViewController : NILauncherViewController

@property (nonatomic, strong) UIImageView *splashImageView;

@end
