//
//  TTSponsor.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/9/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TTSponsor : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *fax;
@property (nonatomic, strong) NSString *tel;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *web;
@property (nonatomic, strong) UIImage *bannerImage;

+ (TTSponsor *)sponsorWithName:(NSString *)name address:(NSString *)address fax:(NSString *)fax tel:(NSString *)tel email:(NSString *)email web:(NSString *)web bannerImage:(UIImage *)bannerImage;

@end
