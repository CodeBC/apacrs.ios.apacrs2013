//
//  TTProgramsMenuViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/30/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTProgramsMenuViewController.h"
#import "TTProgramsMenuCell.h"

#import "TTProgramsViewController.h"
#import "TTProgramsByGroupViewController.h"

#import "TTFreepapersViewController.h"
#import "TTPosterCategoriesViewController.h"
#import "TTFilmFestivalCategoriesViewController.h"

#import "TTProgramOverviewViewController.h"

@interface TTProgramsMenuViewController () <UITableViewDelegate>

@property (nonatomic,strong) NITableViewModel *model;
@property (nonatomic,strong) NICellFactory *cellFactory;

@end

@implementation TTProgramsMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.cellFactory mapObjectClass:[NITitleCellObject class] toCellClass:[TTProgramsMenuCell class]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // /programs/cornea
    // /programs/aacgc
    // /programs/nursing
    // /programs/courses
    // /programs/lunch
    
    NSArray *items = @[
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"Program Overview", nil)],//0
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"Program by Day", nil)],//1
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"Cornea Day (CD)", nil)],//2
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"AACGC Satellite Meeting (GS)", nil)],//3
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"Courses & Wetlabs (CIC/MC/WL)", nil)],//4
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"Film Festival (F)", nil)],//5
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"Free Papers (FP)", nil)],//6
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"Posters (P)", nil)],//7
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"Nursing & Allied Health (NS)", nil)],//8
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"Industry Symposia (IS)", nil)]//9
                       ];
    
    self.tableModel = [[NITableViewModel alloc] initWithListArray:items delegate:self.cellFactory];
    self.title = NSLocalizedString(@"APACRS Program", nil);
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *viewController;
    switch (indexPath.row) {
        case 0:
            viewController = [[TTProgramOverviewViewController alloc] initWithNibName:nil bundle:nil];
            break;
        case 1:
            viewController = [[TTProgramsViewController alloc] initWithNibName:nil bundle:nil];
            break;
        case 2:
            viewController = [[TTProgramsByGroupViewController alloc] initWithGroupId:@36];
            break;
        case 3:
            viewController = [[TTProgramsByGroupViewController alloc] initWithGroupId:@37];
            break;
        case 4:
            viewController = [[TTProgramsByGroupViewController alloc] initWithGroupId:@39];
            break;
        case 5:
            viewController = [[TTFilmFestivalCategoriesViewController alloc] initWithNibName:nil bundle:nil];
            break;
        case 6:
            viewController = [[TTFreepapersViewController alloc] initWithNibName:nil bundle:nil];
            break;
        case 7:
            viewController = [[TTPosterCategoriesViewController alloc] initWithNibName:nil bundle:nil];
            break;
        case 8:
            viewController = [[TTProgramsByGroupViewController alloc] initWithGroupId:@38];
            break;
        case 9:
            viewController = [[TTProgramsByGroupViewController alloc] initWithGroupId:@40];
            break;
        default:
            viewController = [[TTProgramsViewController alloc] initWithNibName:nil bundle:nil];
            break;
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
