//
//  Poster.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/29/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Speaker;

@interface Poster : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * number;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * synopsis;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSNumber * categoryId;
@property (nonatomic, retain) Speaker *speaker;

@end
