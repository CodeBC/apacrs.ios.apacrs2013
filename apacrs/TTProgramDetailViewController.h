//
//  TTProgramDetailViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/25/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTHTMLTemplateViewController.h"

@class Program;

@interface TTProgramDetailViewController : TTHTMLTemplateViewController

@property (nonatomic, strong) Program *program;

@end
