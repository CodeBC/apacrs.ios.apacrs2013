//
//  TTInformationMenuViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/13/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTInformationMenuViewController.h"
#import "TTInformationViewController.h"
#import "TTMapsViewController.h"
#import "TTMapViewController.h"
#import "TTTitleCell.h"
#import "TTSplashViewController.h"

#import "TTAppTheme.h"

@interface TTInformationMenuViewController ()

@end

@implementation TTInformationMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.cellFactory mapObjectClass:[NITitleCellObject class] toCellClass:[TTTitleCell class]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *items = @[
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"Maps of Suntec Singapore", nil)],
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"Conference Contacts", nil)],
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"SNEC Open House", nil)],
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"Hotels", nil)],
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"Taxi", nil)],
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"Emergencies", nil)],
                       [NITitleCellObject objectWithTitle:NSLocalizedString(@"MRT System Map", nil)],
                       ];
    self.tableModel = [[NITableViewModel alloc] initWithListArray:items delegate:self.cellFactory];
    self.title = NSLocalizedString(@"Information", nil);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTInformationViewController *viewController = [[TTInformationViewController alloc] init];
    TTMapViewController *tvc = [[TTMapViewController alloc] init];
    TTSplashViewController *splash = [[TTSplashViewController alloc] init];
    
    NITitleCellObject *object = [self.tableModel objectAtIndexPath:indexPath];
    viewController.title = object.title;
    
    switch (indexPath.row) {
        case 0:
            viewController = [[TTMapsViewController alloc] init];
            [self.navigationController pushViewController:viewController animated:YES];
            break;
        case 1:
            viewController.group = @"Conference";
            [self.navigationController pushViewController:viewController animated:YES];
            break;
        case 2:
            viewController.group = @"SNEC Open House";
            [self.navigationController pushViewController:viewController animated:YES];
            break;
        case 3:
            viewController.group = @"Hotel";
            [self.navigationController pushViewController:viewController animated:YES];
            break;
        case 4:
            viewController.group = @"Taxi";
            [self.navigationController pushViewController:viewController animated:YES];
            break;
        case 5:
            viewController.group = @"Emergencies";
            [self.navigationController pushViewController:viewController animated:YES];
            break;
        case 6:
            tvc.title = @"MRT System Map";
            tvc.image = [UIImage imageNamed:@"train"];
            [self.navigationController pushViewController:tvc animated:YES];
            break;
        case 7:
            [self.navigationController pushViewController:splash animated:YES];
            break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView * childView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 115)];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 20, 240, 50)];
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.font = [TTAppTheme regularFontWithSize:15];
    textLabel.numberOfLines = 5;
    textLabel.textAlignment = NSTextAlignmentCenter;
    
    textLabel.text = @"All timings are in Singapore time (GMT +8)";
    [childView addSubview:textLabel];
    
    UIImageView * bcLogoView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bc"]];
    bcLogoView.frame = CGRectMake(60, CGRectGetMaxY(textLabel.frame) + 10, 200, 35);
    bcLogoView.contentMode = UIViewContentModeScaleToFill;
    
    childView.backgroundColor = [[TTAppTheme cellBackgroundColor] colorWithAlphaComponent:0.85];
    [childView addSubview:bcLogoView];
    
    return childView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 115;
}


@end
