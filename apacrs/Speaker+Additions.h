//
//  Speaker+Additions.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Speaker.h"

@interface Speaker (Additions)

- (NSComparisonResult)compare:(Speaker *)anotherSpeaker usingSortOrder:(NSArray *)sortOrder;

@end

@interface NSArray (SpeakerAddition)

- (NSArray *)groupedSpeakerByName;
- (NSArray *)groupedSpeakerByCountry;
- (NSArray *)speakerNamesWithFormatString:(NSString *)string;

@end
