//
//  TTVenueViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/13/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTVenueViewController.h"
#import "TTFloorPlanViewController.h"
#import "Venue.h"
#import "TTLocationCell.h"
#import "UIImage+Addition.h"
#import "TTAppTheme.h"

@interface TTVenueViewController () <UISearchDisplayDelegate, UITableViewDelegate>

@property (nonatomic, strong) NITableViewModel *venueListDataSource;
@property (nonatomic, strong) UISearchDisplayController *retainSearchDisplayController; // fix the bug Apple ?
@property (nonatomic, strong) UIWebView *floorPlanView;
@property (nonatomic, strong) UISegmentedControl *segmentControl;

@property (nonatomic, strong) NITableViewModel *searchTableModel;

@end

@implementation TTVenueViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.cellFactory mapObjectClass:[Venue class] toCellClass:[TTLocationCell class]];
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    // Segmented Control

    UISegmentedControl *segmentControl = [[UISegmentedControl alloc] initWithItems:@[@"Floor Plan", @"Exhibitor List"]];
    segmentControl.segmentedControlStyle = UISegmentedControlStyleBar;
    [segmentControl sizeToFit];
    segmentControl.frame = CGRectMake(10, 5, self.view.width - 20, segmentControl.height);
    [segmentControl addTarget:self action:@selector(didSwitchMode:) forControlEvents:UIControlEventValueChanged];
    [segmentControl setSelectedSegmentIndex:0];
    [segmentControl setBackgroundColor:[UIColor whiteColor]];
    [segmentControl setTintColor:[TTAppTheme darkColor]];
    segmentControl.layer.cornerRadius = 5.0f;
    segmentControl.layer.borderWidth = 1.0f;
    segmentControl.layer.borderColor = [[TTAppTheme darkColor] CGColor];

    [segmentControl setTitleTextAttributes:@{
                                             UITextAttributeFont: [TTAppTheme regularFontWithSize:14],
                                             UITextAttributeTextColor: [UIColor blackColor],
                                             UITextAttributeTextShadowColor: [UIColor clearColor]
                                             }
                                  forState:UIControlStateNormal];
    
    [segmentControl setTitleTextAttributes:@{
                                             UITextAttributeTextColor: [UIColor whiteColor],
                                             UITextAttributeTextShadowColor: [UIColor clearColor]
                                             }
                                  forState:UIControlStateSelected];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7) {
    
        UIImage *normalBackgroundImage = [UIImage imageWithColor:[UIColor clearColor] size:CGSizeMake(10.0f, 40.0f) andRoundSize:5.0f];
        UIImage *selectedBackgroundImage = [UIImage imageWithColor:[TTAppTheme darkColor] size:CGSizeMake(10.0f, 40.0f) andRoundSize:5.0f];
        
        [segmentControl setBackgroundImage:normalBackgroundImage
                                  forState:UIControlStateNormal
                                barMetrics:UIBarMetricsDefault];
        [segmentControl setBackgroundImage:selectedBackgroundImage
                                  forState:UIControlStateSelected
                                barMetrics:UIBarMetricsDefault];
        [segmentControl setDividerImage:[UIImage imageWithColor:[TTAppTheme darkColor] size:CGSizeMake(1, 10)]
                    forLeftSegmentState:UIControlStateSelected
                      rightSegmentState:UIControlStateNormal
                             barMetrics:UIBarMetricsDefault];
        [segmentControl setDividerImage:[UIImage imageWithColor:[TTAppTheme darkColor] size:CGSizeMake(1, 10)]
                    forLeftSegmentState:UIControlStateNormal
                      rightSegmentState:UIControlStateSelected
                             barMetrics:UIBarMetricsDefault];
        [segmentControl setDividerImage:[UIImage imageWithColor:[TTAppTheme darkColor] size:CGSizeMake(1, 10)]
                    forLeftSegmentState:UIControlStateNormal
                      rightSegmentState:UIControlStateNormal
                             barMetrics:UIBarMetricsDefault];
        
    }
    
    [self.view addSubview:segmentControl];
    _segmentControl = segmentControl;
    
    // Search Bar
    
    UISearchBar *searchBar = [[UISearchBar alloc] init];
    if ([searchBar respondsToSelector:@selector(setSearchBarStyle:)]) {
        searchBar.searchBarStyle = UISearchBarStyleMinimal;
    }
    [searchBar sizeToFit];
    
    _retainSearchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    _searchTableModel = [[NITableViewModel alloc] init];
    
    self.searchDisplayController.delegate = self;
    self.searchDisplayController.searchResultsDataSource = _searchTableModel;
    self.searchDisplayController.searchResultsDelegate = self;
    
    searchBar.frame = CGRectMake(0, CGRectGetMaxY(segmentControl.frame) + 5, searchBar.width, searchBar.height);
    
    [self.view addSubview:searchBar];
    
    // Floor Plan View
    
    _floorPlanView = [[UIWebView alloc] initWithFrame:CGRectZero];
    _floorPlanView.frame = CGRectMake(0,
                                      CGRectGetMaxY(searchBar.frame),
                                      self.view.width,
                                      self.view.height - CGRectGetMaxY(searchBar.frame));
    _floorPlanView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:_floorPlanView];
    
    // Location List Table View
    
    self.tableView.frame = _floorPlanView.frame;
    if ([self.tableView respondsToSelector:@selector(setTintColor:)]) {
        self.tableView.tintColor = [TTAppTheme darkColor];
    }
    
    NSArray *venues = [Venue MR_findAll];
    _venueListDataSource = [[NITableViewModel alloc] initWithSectionedArray:[venues groupedVenueByName] delegate:self.cellFactory];
    [_venueListDataSource setSectionIndexType:NITableViewModelSectionIndexAlphabetical showsSearch:NO showsSummary:YES];
    self.tableModel = _venueListDataSource;
    
    [self showFloorPlan];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = NSLocalizedString(@"Exhibition", nil);
    
    NSURL *htmlURL = [[NSBundle mainBundle] URLForResource:@"venue/venue" withExtension:@"html"];
    [_floorPlanView loadRequest:[NSURLRequest requestWithURL:htmlURL]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showFloorPlan
{
    self.tableView.hidden = YES;
    _floorPlanView.hidden = NO;
}

- (void)showLocationList
{
    self.tableView.hidden = NO;
    _floorPlanView.hidden = YES;
}

- (void)didSwitchMode:(id)sender
{
    switch (_segmentControl.selectedSegmentIndex) {
        case 0:
            [self showFloorPlan];
            break;
        case 1:
            [self showLocationList];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    headerView.backgroundColor = [TTAppTheme tableSectionHeaderColor];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectInset(headerView.bounds, 8, 3)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [TTAppTheme mediumFontWithSize:14];
    label.text = [tableView.dataSource tableView:tableView titleForHeaderInSection:section];
    
    [headerView addSubview:label];
    
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITableViewModel *model = (NITableViewModel *)tableView.dataSource;
    Venue *venue = [model objectAtIndexPath:indexPath];
    
    [_floorPlanView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"window.showTooltipFor('%@')", venue.boothNo]];
    [self showFloorPlan];
    _segmentControl.selectedSegmentIndex = 0;
    
    if (self.searchDisplayController.active) {
        [self.searchDisplayController setActive:NO animated:YES];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"exhibitorName", searchString];
    NSArray *venues = [Venue MR_findAllWithPredicate:predicate];

    _searchTableModel = [[NITableViewModel alloc] initWithSectionedArray:[venues groupedVenueByName] delegate:self.cellFactory];
    [_searchTableModel setSectionIndexType:NITableViewModelSectionIndexAlphabetical showsSearch:NO showsSummary:YES];
    self.searchDisplayController.searchResultsDataSource = _searchTableModel;
    
    return YES;
}

- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        [UIView animateWithDuration:.1 animations:^{
            controller.searchBar.top = 20;
            _segmentControl.alpha = 0;
        }];
    }
}

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        [UIView animateWithDuration:.1 animations:^{
            controller.searchBar.top = CGRectGetMaxY(_segmentControl.frame) + 5;
            _segmentControl.alpha = 1.0;
        }];
    }
}

@end
