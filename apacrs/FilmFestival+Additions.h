//
//  FilmFestival+Additions.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "FilmFestival.h"

@interface FilmFestival (Additions)

+ (NSArray *)categoryNames;

@end
