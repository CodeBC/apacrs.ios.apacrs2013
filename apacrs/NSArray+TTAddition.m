//
//  NSArray+Grouping.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/16/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "NSArray+TTAddition.h"

@implementation NSArray (TTAddition)

- (NSArray *)groupedByKeyPath:(NSString *)keypath defaultGroup:(NSString *)defaultGroup
{
    NSMutableArray *sectioned = [NSMutableArray new];
    NSMutableDictionary *grouped = [NSMutableDictionary new];
    for (id object in self) {
        NSString *value = [object valueForKeyPath:keypath];
        
        NSString *group;
        
        if (value.length) {
            group = [[value substringToIndex:1] capitalizedString];
        } else {
            group = defaultGroup;
        }
        NSMutableArray *items = grouped[group];
        if (!items) {
            items = [NSMutableArray new];
            grouped[group] = items;
        }
        [items addObject:object];
    }
    
    NSArray *keys = [[grouped allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    for (NSString *key in keys) {
        [sectioned addObject:key];
        NSArray *items = grouped[key];
        [sectioned addObjectsFromArray:items];
    }
    
    return sectioned;
}

- (NSArray *)groupedByKeyPath:(NSString *)keypath
{
    return [self groupedByKeyPath:keypath defaultGroup:@" "];
}

- (NSArray *)arrayOfKeyPath:(NSString *)keypath
{
    NSMutableArray *values = [NSMutableArray new];
    for (id object in self) {
        id value = [object valueForKeyPath:keypath];
        if ([values containsObject:value]) {
            continue;
        }
        [values addObject:value];
    }
    return values;
}

@end
