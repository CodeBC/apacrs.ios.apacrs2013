//
//  TTButtonCellObject.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/9/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTButtonCellObject.h"
#import "TTButtonCell.h"

@implementation TTButtonCellObject

- (id)initWithTitle:(NSString *)title image:(UIImage *)image {
    if ((self = [self initWithCellClass:[TTButtonCell class] userInfo:nil])) {
        self.title = title;
        self.image = image;
    }
    return self;
}

@end
