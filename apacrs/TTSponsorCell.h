//
//  TTSponsorCell.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/9/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTTitleCell.h"

@interface TTSponsorCell : TTTitleCell

@property (nonatomic, strong) UIImageView *backgroundImageView;

@end
