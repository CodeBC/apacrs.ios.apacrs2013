//
//  TTFilmFestivalsViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTFilmFestivalsViewController.h"
#import "FilmFestival.h"
#import "TTFilmFestivalCell.h"
#import "TTFilmFestivalDetailViewController.h"

@interface TTFilmFestivalsViewController ()

@end

@implementation TTFilmFestivalsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.cellFactory mapObjectClass:[FilmFestival class] toCellClass:[TTFilmFestivalCell class]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = self.category;
    
    NSArray *items = [FilmFestival MR_findByAttribute:@"category" withValue:self.category andOrderBy:@"number" ascending:YES];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"%K = %@",
                              @"isHidden", [NSNumber numberWithBool:NO]];
    items = [items filteredArrayUsingPredicate:predicate];
    self.tableModel = [[NITableViewModel alloc] initWithListArray:items delegate:self.cellFactory];
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FilmFestival *ffItem = [self.tableModel objectAtIndexPath:indexPath];
    TTFilmFestivalDetailViewController *ffDetailVC = [[TTFilmFestivalDetailViewController alloc] init];
    ffDetailVC.filmFestival = ffItem;
    [self.navigationController pushViewController:ffDetailVC animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
