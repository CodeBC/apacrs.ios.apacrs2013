//
//  TTMenuViewCell.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 8/7/14.
//  Copyright (c) 2014 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTMenuViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *titleLabel;

@end
