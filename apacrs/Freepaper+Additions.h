//
//  Freepaper+Additions.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Freepaper.h"

@interface Freepaper (Additions)

+ (NSArray *)dateTitles;
- (NSString *)dateString;
- (NSString *)programTime;

@end
