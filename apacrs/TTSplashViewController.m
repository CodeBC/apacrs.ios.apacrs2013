//
//  TTSplashViewController.m
//  apacrs
//
//  Created by Ye Myat Min on 7/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTSplashViewController.h"

@interface TTSplashViewController ()

@end

@implementation TTSplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    self.photoView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height - self.navigationController.navigationBar.height)];
    self.photoView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:self.photoView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"About", nil);
    
    self.photoView.image = [UIImage imageNamed:@"Default"];
}

@end
