//
//  TTSponsorDetailsViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/11/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TTSponsor;

@interface TTSponsorDetailsViewController : UIViewController

@property (nonatomic, strong) TTSponsor *sponsor;
@property (nonatomic, strong) UITableView *tableView;

@end
