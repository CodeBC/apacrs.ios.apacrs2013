//
//  TTAppTheme.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 8/8/14.
//  Copyright (c) 2014 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TTAppTheme : NSObject

+ (UIFont *)regularFontWithSize:(CGFloat)size;
+ (UIFont *)mediumFontWithSize:(CGFloat)size;
+ (UIFont *)lightFontWithSize:(CGFloat)size;
+ (UIFont *)regularCondensedFontWithSize:(CGFloat)size;
+ (UIColor *)cellBackgroundColor;
+ (UIColor *)cellSidebarColor;
+ (UIColor *)backgroundColor;
+ (UIColor *)darkColor;
+ (UIColor *)tableSectionHeaderColor;

@end
