//
//  TTAppTheme.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 8/8/14.
//  Copyright (c) 2014 myOpenware. All rights reserved.
//

#import "TTAppTheme.h"

@implementation TTAppTheme

+ (UIFont *)regularFontWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"AvenirNext-Regular" size:size];
}

+ (UIFont *)mediumFontWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"AvenirNext-Medium" size:size];
}

+ (UIFont *)lightFontWithSize:(CGFloat)size;
{
    return [UIFont fontWithName:@"AvenirNext-UltraLight" size:size];
}

+ (UIFont *)regularCondensedFontWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"AvenirNextCondensed-Regular" size:size];
}

+ (UIColor *)cellBackgroundColor
{
    return [self backgroundColor];
}

+ (UIColor *)cellSidebarColor
{
    return [UIColor colorWithRed:0.404 green:0.706 blue:0.780 alpha:1.000];
}

+ (UIColor *)backgroundColor
{
    return [UIColor colorWithRed:0.910 green:0.976 blue:0.976 alpha:1.000];
}

+ (UIColor *)darkColor
{
    return [UIColor colorWithRed:0.004 green:0.510 blue:0.635 alpha:1.000];
}

+ (UIColor *)tableSectionHeaderColor
{
    return [UIColor colorWithRed:0.204 green:0.608 blue:0.710 alpha:1.000];
}

@end
