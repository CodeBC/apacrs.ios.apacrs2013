//
//  TTWebViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/4/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTWebViewController : UIViewController

@property (nonatomic, strong) UIWebView *webView;

@end
