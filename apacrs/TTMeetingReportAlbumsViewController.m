//
//  TTMeetingReportAlbumsViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/15/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTMeetingReportAlbumsViewController.h"
#import "Album.h"
#import "TTAlbumCell.h"
#import "TTPhotoAlbumViewController.h"

@interface TTMeetingReportAlbumsViewController ()

@end

@implementation TTMeetingReportAlbumsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Albums", nil);
        [self.cellFactory mapObjectClass:[Album class] toCellClass:[TTAlbumCell class]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *items = [Album MR_findAll];
    NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *sorted = [items sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];

    self.tableModel = [[NITableViewModel alloc] initWithListArray:sorted delegate:self.cellFactory];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTPhotoAlbumViewController *viewController = [[TTPhotoAlbumViewController alloc] init];
    viewController.photos = [[[self.tableModel objectAtIndexPath:indexPath] photos] allObjects];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
