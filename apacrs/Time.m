//
//  Time.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/25/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Time.h"
#import "NSArray+TTAddition.h"

@implementation Time

@dynamic id;
@dynamic name;
@dynamic programs;

@end