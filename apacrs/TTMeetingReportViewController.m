//
//  TTMeetingReportViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTMeetingReportViewController.h"
#import "TTMeetingReportArticlesViewController.h"
#import "TTMeetingReportAlbumsViewController.h"
#import "UIImage+Addition.h"
#import "TTAppTheme.h"

@interface TTMeetingReportViewController ()

@end

@implementation TTMeetingReportViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.viewControllers = @[
                                 [[TTMeetingReportArticlesViewController alloc] init],
                                 [[TTMeetingReportAlbumsViewController alloc] init]
                                 ];
        self.title = NSLocalizedString(@"Meeting News", nil);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark -

- (void)selectTabButton:(UIButton *)button
{
	UIImage *image = [UIImage imageWithColor:[[TTAppTheme darkColor] colorWithAlphaComponent:0.8] size:CGSizeMake(1, 1)];
	[button setBackgroundImage:image forState:UIControlStateNormal];
	[button setBackgroundImage:image forState:UIControlStateHighlighted];
	
	[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [[button titleLabel] setFont:[TTAppTheme regularFontWithSize:14]];
}

- (void)deselectTabButton:(UIButton *)button
{   
	UIImage *image = [UIImage imageWithColor:[[TTAppTheme backgroundColor] colorWithAlphaComponent:0.95] size:CGSizeMake(1, 1)];
	[button setBackgroundImage:image forState:UIControlStateNormal];
	[button setBackgroundImage:image forState:UIControlStateHighlighted];
    
	[button setTitleColor:[TTAppTheme darkColor] forState:UIControlStateNormal];
    [[button titleLabel] setFont:[TTAppTheme regularFontWithSize:14]];
}

- (CGFloat)tabBarHeight
{
	return 30.0f;
}

@end
