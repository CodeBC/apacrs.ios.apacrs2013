//
//  Freepaper.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 6/21/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Location, Speaker, Time, Topic;

@interface Freepaper : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * endTime;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * startTime;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * isHidden;
@property (nonatomic, retain) Location *location;
@property (nonatomic, retain) NSSet *speakers;
@property (nonatomic, retain) Time *time;
@property (nonatomic, retain) NSSet *topics;
@end

@interface Freepaper (CoreDataGeneratedAccessors)

- (void)addSpeakersObject:(Speaker *)value;
- (void)removeSpeakersObject:(Speaker *)value;
- (void)addSpeakers:(NSSet *)values;
- (void)removeSpeakers:(NSSet *)values;

- (void)addTopicsObject:(Topic *)value;
- (void)removeTopicsObject:(Topic *)value;
- (void)addTopics:(NSSet *)values;
- (void)removeTopics:(NSSet *)values;

@end
