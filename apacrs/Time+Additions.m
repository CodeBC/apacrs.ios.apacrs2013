//
//  Time+Additions.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Time+Additions.h"

@implementation NSArray (Additions)

NSInteger compareTimeOfTheDay(NSString *time1, NSString *time2, void *context)
{
    static NSDictionary *timeValueMap;
    if (!timeValueMap) {
        timeValueMap = @{
                         @"Morning": @0,
                         @"Afternoon" : @1,
                         @"Evening" : @2
                         };
    }
    
    if (timeValueMap[time1] == nil || timeValueMap[time2] == nil) {
        return 0;
    }
    
    return [timeValueMap[time1] integerValue] > [timeValueMap[time2] integerValue];
}

- (NSArray *)timeTitles
{
    NSMutableArray *titles = [NSMutableArray new];
    for (Time *time in self) {
        [titles addObject:time.name];
    }
    [titles sortUsingFunction:&compareTimeOfTheDay context:NULL];
    return titles;
}

@end
