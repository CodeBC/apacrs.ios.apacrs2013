//
//  TTSpeakerCell.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/16/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTSpeakerCell.h"
#import "Speaker.h"
#import "TTAppTheme.h"

@implementation TTSpeakerCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:reuseIdentifier];
    if (self) {
        self.detailTextLabel.backgroundColor = [UIColor clearColor];
        self.detailTextLabel.font = [TTAppTheme regularCondensedFontWithSize:13];
        self.detailTextLabel.textColor = [UIColor colorWithWhite:0.391 alpha:1.000];
        
        self.imageView.layer.masksToBounds = YES;
        self.imageView.layer.borderWidth = 1;
        self.imageView.layer.borderColor = [[UIColor grayColor] CGColor];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return self;
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    if ([object isKindOfClass:[Speaker class]]) {
        Speaker *speaker = (Speaker *)object;
        self.textLabel.text = speaker.name;
        self.detailTextLabel.text = speaker.country;
        NSString *imageFileName = [speaker.imageURL lastPathComponent];
        UIImage *speakerImage = [UIImage imageNamed:[NSString stringWithFormat:@"speakers/%@", imageFileName]];
        if (speakerImage) {
            [self.imageView setImage:speakerImage];
        } else {
            [self.imageView sd_setImageWithURL:[NSURL URLWithString:speaker.imageURL]
                              placeholderImage:[UIImage imageNamed:@"PersonPlaceholder"]
                                       options:0
                                     completed:nil];
        }
    }
    return YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imageView.height = self.imageView.width;
    self.imageView.layer.cornerRadius = self.imageView.width / 2;
    
    self.imageView.center = CGPointMake(self.imageView.center.x, self.contentView.height / 2);
}

@end
