//
//  Poster+Additions.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/29/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Poster+Additions.h"

@implementation Poster (Additions)

+ (NSArray *)categoryNames
{
    NSMutableArray *categoryNmaes = [NSMutableArray new];
    NSArray *items = [Poster MR_findAll];
    for (Poster *item in items) {
        if ([categoryNmaes containsObject:item.category]) {
            continue;
        }
        [categoryNmaes addObject:item.category];
    }
    return [categoryNmaes sortedArrayUsingSelector:@selector(compare:)];
}

@end
