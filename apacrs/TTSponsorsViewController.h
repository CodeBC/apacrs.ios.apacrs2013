//
//  TTSponsorsViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/9/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTSponsorsViewController : UIViewController

@property (nonatomic, strong) UITableView *tableView;

@end
