//
//  TTMultilineTextCell.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/11/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTMultilineTextCell.h"

@implementation TTMultilineTextCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.textLabel.numberOfLines = 3;
}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    const NSUInteger numberOfLines = 3;
    return (44.0 + (numberOfLines - 1) * 19.0);
}

@end
