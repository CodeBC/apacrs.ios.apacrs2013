//
//  Speaker.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Speaker.h"
#import "FilmFestival.h"
#import "Freepaper.h"
#import "Poster.h"
#import "Program.h"
#import "Topic.h"


@implementation Speaker

@dynamic country;
@dynamic id;
@dynamic imageURL;
@dynamic name;
@dynamic programs;
@dynamic topics;
@dynamic freepapers;
@dynamic posters;
@dynamic filmFestivals;

@end
