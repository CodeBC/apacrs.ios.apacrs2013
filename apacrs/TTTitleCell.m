//
//  TTTitleCell.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/29/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTTitleCell.h"

#import "TTAppTheme.h"

@implementation TTTitleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = [[UIView alloc] init];
        self.backgroundView.backgroundColor = [TTAppTheme cellBackgroundColor];
        self.contentView.backgroundColor = [TTAppTheme cellBackgroundColor];
        
        self.textLabel.font = [TTAppTheme regularFontWithSize:14];
        self.textLabel.backgroundColor = [UIColor clearColor];
        self.textLabel.textColor = [UIColor colorWithWhite:0.208 alpha:1.000];
        
        self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
        self.selectedBackgroundView.backgroundColor = [UIColor colorWithRed:0.771 green:0.828 blue:0.828 alpha:1.000];
    }
    return self;
}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    return 60.0f;
}

- (void)updateColorsForState:(BOOL)state
{
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    [self updateColorsForState:selected];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    
    [self updateColorsForState:highlighted];
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    NITitleCellObject *titleObject = (NITitleCellObject *)object;
    if ([titleObject isKindOfClass:[NITitleCellObject class]]) {
        self.textLabel.text = titleObject.title;
    }
    return YES;
}

@end
