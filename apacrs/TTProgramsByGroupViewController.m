//
//  TTProgramsByGroupViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTProgramsByGroupViewController.h"
#import "Program.h"
#import "Program+Additions.h"
#import "Time.h"
#import "Time+Additions.h"
#import "NSString+DateFormatAddition.h"
#import "NSArray+TTAddition.h"

@interface TTProgramsByGroupViewController ()

@end

@implementation TTProgramsByGroupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithGroupId:(NSNumber *)groupId
{
    self = [self initWithNibName:nil bundle:nil];
    if (self) {
        self.groupId = groupId;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = [[[self allPrograms] lastObject] programGroup];
}

- (NSArray *)allPrograms
{
    NSArray *allPrograms = [Program MR_findByAttribute:@"programGroupId" withValue:self.groupId];
    return allPrograms;
}

- (NSArray *)programsForSelectedDateAndTime
{
    NSString *dayTitle = self.dateChooser.sectionTitles[self.dateChooser.selectedSegmentIndex];
    NSString *timeTitle = self.timeChooser.sectionTitles[self.timeChooser.selectedSegmentIndex];
    NSPredicate *predicate;
    if ([timeTitle isEqualToString:@"All"]) {
        predicate = [NSPredicate predicateWithFormat:@"self IN %@ AND %K = %@ AND %K = %@",
                     [self allPrograms],
                     @"date", [dayTitle dateValue],
                     @"isHidden", [NSNumber numberWithBool:NO]];
    } else {
        predicate = [NSPredicate predicateWithFormat:@"self IN %@ AND %K = %@ AND ANY %K = %@ AND %K = %@",
                     [self allPrograms],
                     @"date", [dayTitle dateValue],
                     @"time.name", timeTitle,
                     @"isHidden", [NSNumber numberWithBool:NO]];
    }
    NSArray *events = [Program MR_findAllWithPredicate:predicate];
    return events;
}

- (BOOL)isEmpty
{
    return [self allPrograms].count == 0;
}

- (NSArray *)dateTitles
{
    if ([self isEmpty]) {
        return @[NSLocalizedString(@"N/A", nil)];
    }
    NSArray *dateTitles = [[self allPrograms] dateTitles];
    
    return dateTitles;
}

- (NSArray *)timeTitles
{
    if ([self isEmpty]) {
        return @[NSLocalizedString(@"N/A", nil)];
    }
    NSString *dateTitle = self.dateChooser.sectionTitles[self.dateChooser.selectedSegmentIndex];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self IN %@ AND %K = %@",
                              [self allPrograms],
                              @"date", [dateTitle dateValue]];
    NSArray *programs = [Program MR_findAllWithPredicate:predicate];
    NSArray *times = [programs arrayOfKeyPath:@"time"];
    
    NSMutableArray *titles = [[times timeTitles] mutableCopy];
    NSAssert(titles.count, @"No Time Titles");
    [titles insertObject:@"All" atIndex:0];
    return titles;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
