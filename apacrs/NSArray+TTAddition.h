//
//  NSArray+Grouping.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/16/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (TTAddition)

- (NSArray *)groupedByKeyPath:(NSString *)keypath defaultGroup:(NSString *)defaultGroup;
- (NSArray *)groupedByKeyPath:(NSString *)keypath;
- (NSArray *)arrayOfKeyPath:(NSString *)keypath;

@end
