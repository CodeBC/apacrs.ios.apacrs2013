//
//  TTHTMLTemplateViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/28/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTHTMLTemplateViewController : UIViewController <UIWebViewDelegate>
{
    UIWebView *_webView;
}

@property (nonatomic, strong) UIWebView *webView;

- (NSString *)HTMLFileName;
- (NSString *)HTMLTemplate;
- (NSDictionary *)replaceValues;
- (void)loadHTML;

@end
