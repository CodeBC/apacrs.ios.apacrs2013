//
//  TTSpeakerViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/16/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTSpeakerViewController.h"
#import "TTProgramCell.h"
#import "SVSegmentedControl.h"
#import "NSString+DateFormatAddition.h"
#import "NSArray+TTAddition.h"
#import "TTSpeakerView.h"
#import "TTFavourites.h"

#import "Program.h"
#import "Program+Additions.h"
#import "Speaker.h"
#import "Time.h"
#import "Time+Additions.h"
#import "Topic.h"

#import "TTAppTheme.h"

@interface TTSpeakerViewController ()

@end

@implementation TTSpeakerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.isBookmarked = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Details", nil);
    
    
    NSMutableArray *programs = [TTFavourites favouritedPrograms];
    if([[self allPrograms] count] > 0) {
        Program *program = [[self allPrograms] objectAtIndex:0];
        if ([programs containsObject:program.id]) {
            self.isBookmarked = YES;
        } else {
            self.isBookmarked = NO;
        }
    } else {
        self.isBookmarked = NO;
    }
    
}

- (void)loadView
{
    [super loadView];
    TTSpeakerView *speakerView = [[TTSpeakerView alloc] initWithSpeaker:self.speaker];
    speakerView.backgroundColor = [TTAppTheme backgroundColor];
    speakerView.speakerNameLabel.textColor = [UIColor colorWithWhite:0.333 alpha:1.000];
    speakerView.speakerCountryLabel.textColor = [UIColor lightGrayColor];
    [self.view addSubview:speakerView];
    
    const CGFloat speakerHeaderHeight = 80.0f;
    speakerView.frame = CGRectMake(0, 0, self.view.width, speakerHeaderHeight);
    self.dateScrollView.top = self.dateScrollView.top + speakerHeaderHeight;
    self.tableView.top = (self.tableView.top + speakerHeaderHeight) - self.timeScrollView.height;
    self.tableView.height = self.view.height - (speakerHeaderHeight + self.dateScrollView.height);
    [self.timeScrollView removeFromSuperview];
    
    self.dateScrollView.backgroundColor = [UIColor colorWithRed:0.877 green:0.941 blue:0.941 alpha:1.000];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 42, 40)];
    [button setBackgroundImage:[UIImage imageNamed:@"Fav"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(bookmarkAll) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void)bookmarkAll
{
    NSArray *programsOnDisplay = [self programsForSelectedDateAndTime];
    if(self.isBookmarked) {
        for (Program *program in programsOnDisplay) {
            [TTFavourites favourite:NO program:program];
        }
        self.isBookmarked = NO;
    } else {
        for (Program *program in programsOnDisplay) {
            [TTFavourites favourite:YES program:program];
        }
        self.isBookmarked = YES;
    }
    [self.tableView reloadData];
}

- (NSArray *)allPrograms
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY %K = %@",
                              @"speakers", self.speaker];
    
    NSArray *topics = [Topic MR_findAllWithPredicate:predicate];
    
    predicate = [NSPredicate predicateWithFormat:@"ANY %K = %@ OR ANY %K IN %@",
                 @"speakers", self.speaker,
                 @"topics", topics];
    return [Program MR_findAllWithPredicate:predicate];
}

- (NSArray *)dateTitles
{
    return [[self allPrograms] dateTitles];
}

- (NSArray *)timeTitles
{
    NSString *dateTitle = self.dateChooser.sectionTitles[self.dateChooser.selectedSegmentIndex];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self IN %@ AND %K = %@",
                              [self allPrograms],
                              @"date", [dateTitle dateValue]];
    NSArray *programs = [Program MR_findAllWithPredicate:predicate];
    NSArray *times = [programs arrayOfKeyPath:@"time"];
    
    return [times timeTitles];
}

- (NSArray *)programsForSelectedDateAndTime
{
    NSString *dayTitle = self.dateChooser.sectionTitles[self.dateChooser.selectedSegmentIndex];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self IN %@ AND %K = %@ AND %K = %@",
                              [self allPrograms],
                              @"date", [dayTitle dateValue],
                              @"isHidden", [NSNumber numberWithBool:NO]];
    NSArray *events = [Program MR_findAllWithPredicate:predicate];
    return events;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setSpeaker:(Speaker *)speaker
{
    if (_speaker != speaker) {
        _speaker = speaker;
//        [self reloadEvents];
    }
}

@end
