//
//  TTFilmFestivalDetailViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/29/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTFilmFestivalDetailViewController.h"
#import "FilmFestival.h"
#import "Speaker.h"

@interface TTFilmFestivalDetailViewController ()

@end

@implementation TTFilmFestivalDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.filmFestival) {
        [self loadHTML];
    }
}

- (NSString *)HTMLFileName
{
    // TODO: use other file?
    return @"session";
}

- (void)setFilmFestival:(FilmFestival *)filmFestival
{
    if (filmFestival != _filmFestival) {
        _filmFestival = filmFestival;
        [self loadHTML];
    }
}

- (NSDictionary *)replaceValues
{
    NSMutableDictionary *values;
    values = [@{
              @"Program-Title": self.filmFestival.title ? self.filmFestival.title : @"",
              @"Program-Description": self.filmFestival.synopsis ? self.filmFestival.synopsis : @"",
              @"Program-Speaker": self.filmFestival.speaker ? self.filmFestival.speaker.name : @"",
              } mutableCopy];
    return values;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [super webViewDidFinishLoad:webView];
    self.title = self.filmFestival.title;
}

@end
