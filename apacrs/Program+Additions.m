//
//  Program+Additions.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/28/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Program+Additions.h"

@implementation Program (Additions)

+ (NSArray *)dateTitles
{
    NSArray *programs = [Program MR_findAll];
    return [programs dateTitles];
}

- (NSString *)dateString
{
    static NSDateFormatter *formatter;
    if (!formatter) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [formatter setDateFormat:@"dd MMM (EEE)"];
    }
    
    return [formatter stringFromDate:self.date];
}

+ (NSArray *)typeNames
{
    NSMutableArray *typeNames = [NSMutableArray new];
    NSArray *items = [Program MR_findAll];
    for (Program *item in items) {
        if ([typeNames containsObject:item.type]) {
            continue;
        }
        [typeNames addObject:item.type];
    }
    return typeNames;
}

- (NSString *)programTime
{
    NSString *programTime = @"";
    if (self.startTime && self.endTime) {
        programTime = [NSString stringWithFormat:@"%@ - %@hrs", self.startTime, self.endTime];
    } else if (self.startTime) {
        programTime = self.startTime;
        programTime = [programTime stringByAppendingString:@"hrs"];
    }
    return programTime;
}

@end

@implementation NSArray (ProgramAdditions)

- (NSArray *)dateTitles
{
    NSMutableArray *titles = [[NSMutableArray alloc] init];
    for (Program *program in self) {
        NSString *dateName = program.dateString;
        if ([titles containsObject:dateName]) {
            continue;
        }
        [titles addObject:dateName];
    }
    [titles sortUsingSelector:@selector(caseInsensitiveCompare:)];
    return titles;
}

@end