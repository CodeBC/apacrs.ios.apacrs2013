//
//  FilmFestival+Additions.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "FilmFestival+Additions.h"

@implementation FilmFestival (Additions)

+ (NSArray *)categoryNames
{
    NSMutableArray *categoryNmaes = [NSMutableArray new];
    NSArray *items = [FilmFestival MR_findAll];
    for (FilmFestival *item in items) {
        if ([categoryNmaes containsObject:item.category]) {
            continue;
        }
        [categoryNmaes addObject:item.category];
    }
    return [categoryNmaes sortedArrayUsingSelector:@selector(compare:)];
}

@end
