//
//  TTMenuViewCell.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 8/7/14.
//  Copyright (c) 2014 myOpenware. All rights reserved.
//

#import "TTMenuViewCell.h"
#import "TTAppTheme.h"

@implementation TTMenuViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.imageView = [[UIImageView alloc] init];
        [self.contentView addSubview:self.imageView];
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.font = [TTAppTheme regularFontWithSize:12.0f];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.titleLabel];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.imageView sizeToFit];
    [self.titleLabel sizeToFit];
    
    self.imageView.frame = NIFrameOfCenteredViewWithinView(self.imageView, self.contentView);
    self.titleLabel.frame = NIFrameOfCenteredViewWithinView(self.titleLabel, self.contentView);
    
    self.imageView.top -= self.titleLabel.height / 2;
    self.titleLabel.top = CGRectGetMaxY(self.imageView.frame) + 5;
}

@end
