//
//  Venue.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/8/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Venue.h"
#import "NSArray+TTAddition.h"


@implementation Venue

@dynamic id;
@dynamic exhibitorName;
@dynamic boothNo;

@end

@implementation NSArray (VenueGrouping)

- (NSArray *)groupedVenueByName
{
    NSSortDescriptor *nameDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"exhibitorName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *sorted = [self sortedArrayUsingDescriptors:[NSArray arrayWithObject:nameDescriptor]];
    return [sorted groupedByKeyPath:@"exhibitorName"];
}

@end
