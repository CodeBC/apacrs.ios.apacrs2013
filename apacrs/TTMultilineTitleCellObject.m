//
//  TTMultilineTitleCellObject.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/11/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTMultilineTitleCellObject.h"
#import "TTMultilineTextCell.h"

@implementation TTMultilineTitleCellObject

- (id)initWithTitle:(NSString *)title image:(UIImage *)image {
    if ((self = [self initWithCellClass:[TTMultilineTextCell class] userInfo:nil])) {
        self.title = title;
        self.image = image;
    }
    return self;
}

@end
