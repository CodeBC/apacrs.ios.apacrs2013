//
//  TTLauncherViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/12/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTLauncherViewController.h"
#import "TTProgramsMenuViewController.h"
#import "TTSpeakersViewController.h"
#import "TTVenueViewController.h"
#import "TTSearchViewController.h"
#import "TTMyScheduleViewController.h"
#import "TTSponsorsViewController.h"
#import "TTHighlightsViewController.h"
#import "TTAboutViewController.h"
#import "TTInformationMenuViewController.h"
#import "TTMeetingReportViewController.h"
#import "TTNextMeetingViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "HSLUpdateChecker.h"

#define kModalSeguePushedBackAnimationDuration 0.1
#define kModalSegueBringForwardAnimationDuration 0.3
#define kStatusBarHeight 20.0f

@interface TTLauncherViewController ()

@property (nonatomic, strong) NSTimer *topBannerTimer;
@property (nonatomic, strong) NSTimer *bottomBannerTimer;

@end

#define TOP_BANNER_HEIGHT 92.0f
#define BOTTOM_BANNER_HEIGHT 35.0f
#define BANNER_ROTATION_INTERVAL 3 //seconds

/*
 IPHONE 5
 #define TOP_BANNER_HEIGHT 145.0f
 #define BOTTOM_BANNER_HEIGHT 70.0f
 */

@implementation TTLauncherViewController

- (void)rotateBanner:(NSTimer *)timer
{
    UIScrollView *scrollView = timer.userInfo;
    CGPoint offset = scrollView.contentOffset;
    offset.x += scrollView.width;
    if (offset.x >= scrollView.contentSize.width) {
        [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    } else {
        [scrollView setContentOffset:offset animated:YES];
    }
}

- (void)showAbout
{
    TTAboutViewController *aboutViewController = [[TTAboutViewController alloc] initWithNibName:nil bundle:nil];
    [self presentModalViewControllerInsideNavigationController:aboutViewController];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [HSLUpdateChecker checkForUpdate];
    
    
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setYear:2013];
    [components setDay:12];
    [components setMonth:7];
    [components setHour:12];
    [components setMinute:25];
    [components setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];
    NSDate *firstDate = [calendar dateFromComponents:components];
    
    NSDateComponents *components2 = [[NSDateComponents alloc] init];
    [components2 setYear:2013];
    [components2 setDay:13];
    [components2 setMonth:7];
    [components2 setHour:12];
    [components2 setMinute:25];
    [components2 setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:8]];
    NSDate *secondDate = [calendar dateFromComponents:components2];

    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    //[self scheduleNotificationForDate:firstDate withMessage:@"AMO Lunch Symp \n Surgical Mgt of Astigmatism"];
    //[self scheduleNotificationForDate:secondDate withMessage:@"AMO Lunch Symp \n Predictable Successful IOL Surgery"];
     
    
    self.view = [[UIView alloc] initWithFrame:self.launcherView.frame];
    [self.view addSubview:self.launcherView];
    
    self.launcherView.height = 333.0;
    
    int topHeight = TOP_BANNER_HEIGHT;
    int bottomHeight = BOTTOM_BANNER_HEIGHT;
    
    if([self isTall]) {
        NSLog(@"TALL");
        topHeight = 145.0f;
        bottomHeight = 70.0f;
    }

    self.launcherView.frame = CGRectMake(0, topHeight, self.launcherView.width, self.launcherView.height );
    self.view.backgroundColor = [UIColor colorWithRed:0.11f green:0.09f blue:0.26f alpha:1.00f];
    
    CGRect topBannerFrame = CGRectMake(0, 0, self.view.width, topHeight);
    CGRect bottomBannerFrame = CGRectMake(0, 0, self.view.width, bottomHeight);
    
    UIScrollView *topBannerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, topHeight)];
    UIScrollView *bottomBannerScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,
                                                                                          self.view.height - bottomHeight,
                                                                                          self.view.width,
                                                                                          bottomHeight)];
    topBannerScrollView.pagingEnabled = YES;
    bottomBannerScrollView.pagingEnabled = YES;
    
    topBannerScrollView.showsHorizontalScrollIndicator = NO;
    bottomBannerScrollView.showsHorizontalScrollIndicator = NO;
    
    [self.view addSubview:topBannerScrollView];
    [self.view addSubview:bottomBannerScrollView];
    
    // Add Banners
    NSMutableArray *topBanners = [NSMutableArray new];
    
    UIButton *topBanner1 = [UIButton buttonWithType:UIButtonTypeCustom];
    if([self isTall]) {
        [topBanner1 setBackgroundImage:[UIImage imageNamed:@"top"] forState:UIControlStateNormal];
    } else {
        [topBanner1 setBackgroundImage:[UIImage imageNamed:@"top4"] forState:UIControlStateNormal];
    }
    [topBanner1 addTarget:self action:@selector(showAbout) forControlEvents:UIControlEventTouchUpInside];
    
    [topBanners addObject:topBanner1];
    NSMutableArray *bottomBanners = [NSMutableArray new];

    if([self isTall]) {
        UIImageView *bottomBanner1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bottomBanner5"]];
        UIImageView *bottomBanner2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bottomBannerPlatinum5"]];
        [bottomBanners addObject:bottomBanner1];
        [bottomBanners addObject:bottomBanner2];
    } else {

        UIImageView *bottomBanner1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bottomBanner"]];
        UIImageView *bottomBanner2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bottomBannerPlatinum"]];
        [bottomBanners addObject:bottomBanner1];
        [bottomBanners addObject:bottomBanner2];
    }

    // Top Banners
    {
        CGRect frame = topBannerFrame;
        for (UIView *banner in topBanners) {
            banner.frame = frame;
            frame.origin.x += frame.size.width;
            [topBannerScrollView addSubview:banner];
        }
        
        topBannerScrollView.contentSize = CGSizeMake(topBanners.count * topBannerFrame.size.width, topBannerFrame.size.height);
    }
    
    // Bottom Banners
    {
        CGRect frame = bottomBannerFrame;
        for (UIView *banner in bottomBanners) {
            banner.frame = frame;
            frame.origin.x += frame.size.width;
            [bottomBannerScrollView addSubview:banner];
        }
        
        bottomBannerScrollView.contentSize = CGSizeMake(bottomBanners.count * bottomBannerFrame.size.width, bottomBannerFrame.size.height);
    }
    
    _topBannerTimer = [NSTimer scheduledTimerWithTimeInterval:BANNER_ROTATION_INTERVAL
                                                       target:self
                                                     selector:@selector(rotateBanner:)
                                                     userInfo:topBannerScrollView
                                                      repeats:YES];
    _bottomBannerTimer = [NSTimer scheduledTimerWithTimeInterval:BANNER_ROTATION_INTERVAL
                                                       target:self
                                                     selector:@selector(rotateBanner:)
                                                     userInfo:bottomBannerScrollView
                                                      repeats:YES];
    
    UINavigationBar *navigationBar = [[UINavigationBar alloc] init];
    UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:NSLocalizedString(@"APACRS", nil)];
    [navigationBar pushNavigationItem:item animated:NO];
    [navigationBar sizeToFit];
    
    self.launcherView.contentInsetForPages = UIEdgeInsetsMake(0, 0, 0, 0);
    
    UIView *animationView = [[UIView alloc] init];
    animationView.backgroundColor = [UIColor whiteColor];
    
    [_splashImageView addSubview:animationView];
#ifdef DEBUG
    double delayInSeconds = 0.0;
#else
    double delayInSeconds = 2.0;
#endif
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [_splashImageView removeFromSuperview];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"shownTimingNotice"] == NO) {
            NSString *title = NSLocalizedString(@"Notice", nil);
            NSString *message = NSLocalizedString(@"All timing stated in the application are in +8GMT.", nil);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"Okay", nil)
                                                  otherButtonTitles:nil];
            [alert show];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"shownTimingNotice"];
        }
        
    });
    
    self.launcherView.buttonSize = CGSizeMake(107, 111);
    self.launcherView.numberOfColumns = 3;
    self.launcherView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
}

#pragma mark - NILauncherDataSource

- (NSInteger)launcherView:(NILauncherView *)launcherView numberOfButtonsInPage:(NSInteger)page {
    return 9;
}

- (UIView<NILauncherButtonView> *)launcherView:(NILauncherView *)launcherView buttonViewForPage:(NSInteger)page atIndex:(NSInteger)buttonIndex {
    NSString *const identifier = @"NILauncherView";
    NILauncherButtonView *buttonView = (NILauncherButtonView *)[launcherView dequeueReusableViewWithIdentifier:identifier];
    if (!buttonView) {
        buttonView = [[NILauncherButtonView alloc] initWithReuseIdentifier:identifier];
    }
    static NSArray *titles;
    static NSArray *icons;
    
    if (!titles) {
        titles = @[
                   @"Program",
                   @"Speakers",
                   @"Exhibition",
                   @"Highlights",
                   @"My Schedule",
                   @"Search",
                   @"Meeting News",
                   @"Information",
                   @"Next Meeting"];
    }
    
    if (!icons) {
        icons = @[
                  @"Programs",
                  @"Speakers",
                  @"Venue",
                  @"Highlights",
                  @"Schedule",
                  @"Search",
                  @"MeetingReporter",
                  @"UsefulInfo",
                  @"Next-Meeting-icon",
                  ];
    }
    
    [buttonView.button setImage:[UIImage imageNamed:icons[buttonIndex]] forState:UIControlStateNormal];
    // hack to highlight button background on tap
    [buttonView.button addTarget:self action:@selector(onTouchDown:) forControlEvents:UIControlEventTouchDown];
    [buttonView.button addTarget:self action:@selector(onTouchUp:) forControlEvents:UIControlEventTouchUpInside|UIControlEventTouchUpOutside];
    buttonView.contentInset = UIEdgeInsetsMake(5, 5, 5, 5);
    buttonView.label.text = titles[buttonIndex];
    buttonView.label.textColor = [UIColor colorWithRed:0.15f green:0.15f blue:0.15f alpha:1.00f];
    buttonView.label.font = [UIFont fontWithName:@"TrebuchetMS" size:14.0f];
    buttonView.backgroundColor = UIColorFromString(@"rgba(103,180,199,1)");
    buttonView.layer.borderColor = [UIColor whiteColor].CGColor;
    buttonView.layer.borderWidth = 1.0f;
    return buttonView;
}

- (void)onTouchDown:(UIButton *)button
{
//    NILauncherButtonView *buttonView = (NILauncherButtonView *)[button superview];
//    if ([buttonView isKindOfClass:[NILauncherButtonView class]]) {
//        buttonView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"LauncherButtonBackgroundPushed"]];
//    }
}

- (void)onTouchUp:(UIButton *)button
{
//    NILauncherButtonView *buttonView = (NILauncherButtonView *)[button superview];
//    if ([buttonView isKindOfClass:[NILauncherButtonView class]]) {
//        buttonView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"LauncherButtonBackground"]];
//    }
}

#pragma mark - NILauncherDelegate

- (void)launcherView:(NILauncherView *)launcherView didSelectItemOnPage:(NSInteger)page atIndex:(NSInteger)index
{
    UIViewController *viewController;
    switch (index) {
        case 0:
            viewController = [[TTProgramsMenuViewController alloc] init];
            break;
        case 1:
            viewController = [[TTSpeakersViewController alloc] init];
            break;
        case 2:
            viewController = [[TTVenueViewController alloc] init];
            break;
        case 3:
            viewController = [[TTHighlightsViewController alloc] init];
            break;
        case 4:
            viewController = [[TTMyScheduleViewController alloc] init];
            break;
        case 5:
            viewController = [[TTSearchViewController alloc] init];
            break;
        case 6:
            viewController = [[TTMeetingReportViewController alloc] init];
            break;
        case 7:
            viewController = [[TTInformationMenuViewController alloc] init];
            break;
        case 8:
            viewController = [[TTNextMeetingViewController alloc] init];
            break;
        default:
            viewController = [[TTProgramsViewController alloc] init];
            break;
    }
    
    [self presentModalViewControllerInsideNavigationController:viewController];
}

#pragma mark - View Transition

- (void)presentModalViewControllerInsideNavigationController:(UIViewController *)modalViewController
{
    UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:modalViewController];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 29, 30)];
    [button setBackgroundImage:[UIImage imageNamed:@"MenuButton"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    modalViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [self presentModalViewController:navCon animated:YES];
}

- (void)presentModalViewController:(UIViewController *)modalViewController animated:(BOOL)animated
{
    self.view.userInteractionEnabled = NO;
    
    [self.view.superview addSubview:modalViewController.view];
    
    modalViewController.view.layer.affineTransform = CGAffineTransformMakeScale(0.1, 0.1);
    
    UIViewAnimationOptions options = UIViewAnimationOptionCurveLinear;
    [UIView animateWithDuration:0.3 delay:0 options:options animations:^{
        modalViewController.view.layer.affineTransform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        self.view.userInteractionEnabled = YES;
        [super presentModalViewController:modalViewController animated:NO];
    }];
    
}

- (void)dismissView
{
    UIImageView *presentingView = NISnapshotViewOfView(self.presentedViewController.view);
    presentingView.contentMode = UIViewContentModeScaleToFill;
    presentingView.frame = CGRectMake(0, -kStatusBarHeight, self.view.width, self.view.height+kStatusBarHeight);
    
    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending) {
        presentingView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
    }
    
    [self.view addSubview:presentingView];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    UIViewAnimationOptions options = UIViewAnimationOptionCurveEaseIn;
    [UIView animateWithDuration:0.3 delay:0 options:options animations:^{
        presentingView.frame = CGRectMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds), 1, 1);
    } completion:^(BOOL finished) {
        [presentingView removeFromSuperview];
    }];
}

- (BOOL)isTall
{
    CGRect bounds = [[UIScreen mainScreen] bounds];
    CGFloat height = bounds.size.height;
    CGFloat scale = [[UIScreen mainScreen] scale];
    
    return (([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) && ((height * scale) >= 1136));
}

-(void) scheduleNotificationForDate: (NSDate*)date withMessage: (NSString *)message {
    
    /* Here we cancel all previously scheduled notifications */
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    localNotification.fireDate = date;
    NSLog(@"Notification will be shown on: %@",localNotification.fireDate);
    
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.alertBody = message;
    localNotification.alertAction = NSLocalizedString(@"OK", nil);
    
    /* Here we set notification sound and badge on the app's icon "-1"
     means that number indicator on the badge will be decreased by one
     - so there will be no badge on the icon */
    
    // localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.applicationIconBadgeNumber = -1;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

@end
