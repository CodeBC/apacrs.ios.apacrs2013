//
//  Poster.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/29/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Poster.h"
#import "Speaker.h"


@implementation Poster

@dynamic id;
@dynamic number;
@dynamic title;
@dynamic category;
@dynamic categoryId;
@dynamic speaker;
@dynamic synopsis;

@end
