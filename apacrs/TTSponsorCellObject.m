//
//  TTSponsorCellObject.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/9/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTSponsorCellObject.h"
#import "TTSponsorCell.h"
#import "TTSponsor.h"

@implementation TTSponsorCellObject

- (id)initWithSponsor:(TTSponsor *)sponsor
{
    self = [super initWithCellClass:[TTSponsorCell class]];
    if (self) {
        self.sponsor = sponsor;
    }
    return self;
}

@end
