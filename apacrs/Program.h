//
//  Program.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 6/22/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Location, Speaker, Time, Topic;

@interface Program : NSManagedObject

@property (nonatomic, retain) NSString * altName;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * endTime;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSNumber * isHidden;
@property (nonatomic, retain) NSNumber * isProgramHighlighted;
@property (nonatomic, retain) NSString * keywords;
@property (nonatomic, retain) NSString * order;
@property (nonatomic, retain) NSString * programGroup;
@property (nonatomic, retain) NSNumber * programGroupId;
@property (nonatomic, retain) NSString * startTime;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * speakerOrder;
@property (nonatomic, retain) NSString * topicOrder;
@property (nonatomic, retain) Location *location;
@property (nonatomic, retain) NSSet *speakers;
@property (nonatomic, retain) Time *time;
@property (nonatomic, retain) NSSet *topics;
@end

@interface Program (CoreDataGeneratedAccessors)

- (void)addSpeakersObject:(Speaker *)value;
- (void)removeSpeakersObject:(Speaker *)value;
- (void)addSpeakers:(NSSet *)values;
- (void)removeSpeakers:(NSSet *)values;

- (void)addTopicsObject:(Topic *)value;
- (void)removeTopicsObject:(Topic *)value;
- (void)addTopics:(NSSet *)values;
- (void)removeTopics:(NSSet *)values;

@end
