//
//  TTPickerCellObject.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/7/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTPickerCellObject.h"
#import "TTPickerCell.h"

@implementation TTPickerCellObject

- (Class)cellClass
{
    return [TTPickerCell class];
}

+ (id)pickerObjectWithTitle:(NSString *)title titles:(NSArray *)titles values:(NSArray *)values
{
    TTPickerCellObject *object = [[TTPickerCellObject alloc] init];
    object.title = title;
    object.titles = titles;
    object.values = values;
    
    return object;
}

@end
