//
//  TTSpeakerView.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/28/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTSpeakerView.h"
#import "Speaker.h"

#import "TTAppTheme.h"

@implementation TTSpeakerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.speakerImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        self.speakerNameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.speakerCountryLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        
        self.speakerNameLabel.font = [TTAppTheme regularFontWithSize:15];
        self.speakerCountryLabel.font = [TTAppTheme regularFontWithSize:13];
        self.speakerImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        self.speakerNameLabel.backgroundColor = [UIColor clearColor];
        self.speakerCountryLabel.backgroundColor = [UIColor clearColor];
        
        self.speakerImageView.layer.masksToBounds = YES;
        self.speakerImageView.layer.borderWidth = 1;
        self.speakerImageView.layer.borderColor = [[UIColor grayColor] CGColor];
        self.speakerImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        [self addSubview:self.speakerNameLabel];
        [self addSubview:self.speakerCountryLabel];
        [self addSubview:self.speakerImageView];
    }
    return self;
}

- (id)initWithSpeaker:(Speaker *)speaker
{
    self = [self initWithFrame:CGRectZero];
    if (self) {
        self.speaker = speaker;
    }
    return self;
}

- (void)setSpeaker:(Speaker *)speaker
{
    if (_speaker == speaker) {
        return;
    }
    _speaker = speaker;
    [self.speakerImageView sd_setImageWithURL:[NSURL URLWithString:self.speaker.imageURL]
                             placeholderImage:[UIImage imageNamed:@"PersonPlaceholder"]
                                      options:0];
    self.speakerNameLabel.text = self.speaker.name;
    self.speakerCountryLabel.text = self.speaker.country;
}

- (void)layoutSubviews
{
    const CGFloat speakerHeight = 55.0f;
    const CGFloat speakerWidth = 55.0f;
    const CGFloat labelLeftPadding = 20.0f;
    
    self.speakerImageView.size = CGSizeMake(speakerWidth, speakerHeight);
    self.speakerImageView.top =  NICenterY(self.size, self.speakerImageView.size);
    self.speakerImageView.left = 10.0f;
    self.speakerImageView.layer.cornerRadius = self.speakerImageView.width / 2;
    
    CGFloat labelLeft = labelLeftPadding + self.speakerImageView.left + self.speakerImageView.width;
    
    self.speakerNameLabel.top = self.speakerImageView.top;
    self.speakerNameLabel.left = labelLeft;
    self.speakerCountryLabel.left = labelLeft;
    
    self.speakerNameLabel.width = self.width - labelLeft;
    self.speakerCountryLabel.width = self.width - labelLeft;
    
    self.speakerNameLabel.height = 25;
    self.speakerCountryLabel.height = 25;
    
    self.speakerCountryLabel.top = self.speakerNameLabel.top + self.speakerNameLabel.height;
}

@end
