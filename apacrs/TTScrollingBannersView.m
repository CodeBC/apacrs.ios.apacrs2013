//
//  TTScrollingBannersView.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 8/9/14.
//  Copyright (c) 2014 myOpenware. All rights reserved.
//

#import "TTScrollingBannersView.h"

#define BANNER_ROTATION_INTERVAL 3 //seconds

@interface TTScrollingBannersView ()

@property (nonatomic, strong) NSTimer *bannerTimer;

@end

@implementation TTScrollingBannersView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.scrollView = [[UIScrollView alloc] init];
        self.scrollView.pagingEnabled = YES;
        self.scrollView.showsHorizontalScrollIndicator = NO;
        
        [self addSubview:self.scrollView];
        
        self.bannerTimer = [NSTimer scheduledTimerWithTimeInterval:BANNER_ROTATION_INTERVAL
                                                       target:self
                                                     selector:@selector(rotateBanner:)
                                                     userInfo:self.scrollView
                                                      repeats:YES];
    }
    return self;
}

#pragma mark - Banner Rotation

- (void)rotateBanner:(NSTimer *)timer
{
    UIScrollView *scrollView = timer.userInfo;
    CGPoint offset = scrollView.contentOffset;
    offset.x += scrollView.width;
    if (offset.x >= scrollView.contentSize.width) {
        [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    } else {
        [scrollView setContentOffset:offset animated:YES];
    }
}

#pragma mark - Layout

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.scrollView.frame = self.bounds;
    
    CGRect frame = CGRectMake(0, 0, self.scrollView.width, self.scrollView.height);
    for (UIButton *bannerButton in self.scrollView.subviews) {
        if ([bannerButton isKindOfClass:[UIButton class]] == NO) {
            continue;
        }
        bannerButton.frame = frame;
        frame.origin.x += frame.size.width;
        [self.scrollView addSubview:bannerButton];
    }
        
    self.scrollView.contentSize = CGSizeMake(self.bannerImages.count * self.size.width, self.size.height);
}

#pragma mark - Setting Banner

- (void)setBannerImages:(NSArray *)bannerImages
{
    _bannerImages = bannerImages;
    
    for (UIButton *bannerButton in self.scrollView.subviews) {
        if ([bannerButton isKindOfClass:[UIButton class]] == NO) {
            continue;
        }
        [bannerButton removeFromSuperview];
    }
    
    self.scrollView.contentSize = CGSizeZero;
    
    for (UIImage *bannerImage in self.bannerImages) {
        
        UIButton *bannerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [bannerButton setBackgroundImage:bannerImage forState:UIControlStateNormal];
        [bannerButton setTag:[self.bannerImages indexOfObject:bannerImage]];
        
        if ([self.delegate respondsToSelector:@selector(scrollingBannerView:didTapBannerAtIndex:)]) {
            [bannerButton addTarget:self action:@selector(didTap:) forControlEvents:UIControlEventTouchUpInside];
            [bannerButton setUserInteractionEnabled:YES];
        } else {
            [bannerButton setUserInteractionEnabled:NO];
        }
        
        [self.scrollView addSubview:bannerButton];
    }
    
    [self setNeedsLayout];
}

#pragma mark - Banner Did Tap

- (void)didTap:(id)sender
{
    UIButton *button = sender;
    if ([self.delegate respondsToSelector:@selector(scrollingBannerView:didTapBannerAtIndex:)]) {
        [self.delegate scrollingBannerView:self didTapBannerAtIndex:button.tag];
    }
}

#pragma mark - Delegate

- (void)setDelegate:(id<TTScrollingBannersViewDelegate>)delegate
{
    if (_delegate != delegate) {
        _delegate = delegate;
        
        for (UIButton *bannerButton in self.scrollView.subviews) {
            if ([bannerButton isKindOfClass:[UIButton class]] == NO) {
                continue;
            }
            if ([self.delegate respondsToSelector:@selector(scrollingBannerView:didTapBannerAtIndex:)]) {
                [bannerButton addTarget:self action:@selector(didTap:) forControlEvents:UIControlEventTouchUpInside];
                [bannerButton setUserInteractionEnabled:YES];
            } else {
                [bannerButton setUserInteractionEnabled:NO];
            }
        }
    }
}

#pragma mark -

- (void)prepareForReuse
{
    self.bannerImages = nil;
}

@end
