//
//  TTInformationViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTInformationViewController.h"
#import "Information.h"
#import "Information+Additions.h"

@interface TTInformationViewController () <UIAlertViewDelegate>

@property (nonatomic, strong) NSString *callingPhoneNumber;

@end

@implementation TTInformationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *items = [Information MR_findByAttribute:@"group" withValue:self.group];

    self.tableModel = [[NITableViewModel alloc] initWithListArray:items delegate:(id)[NICellFactory class]];
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Information *info = [self.tableModel objectAtIndexPath:indexPath];
    if(info.contact && [info.contact length] > 0) {
        NSString *phoneNumber = info.contact;
        self.callingPhoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *message = [NSString stringWithFormat:@"Do you want to call %@", phoneNumber];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Call", @"Call confirmation alert title")
                                                       message:message
                                                      delegate:self
                                             cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                             otherButtonTitles:NSLocalizedString(@"Call", nil), nil];
        [alert show];
    } else {
        [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
    }
}

#pragma mark - 
#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", self.callingPhoneNumber];
        NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
    
    [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
