//
//  Album.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/18/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Album.h"
#import "Photo.h"


@implementation Album

@dynamic title;
@dynamic id;
@dynamic photos;

@end
