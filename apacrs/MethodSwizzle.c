//
//  MethodSwizzle.c
//  Baydin
//
//  Created by Thant Thet Khin Zaw on 2/18/13.
//  Copyright (c) 2013 Thant Thet Khin Zaw. All rights reserved.
//

#include <objc/runtime.h>
#include "MethodSwizzle.h"

void Swizzle(Class c, SEL orig, SEL new)
{
    Method origMethod = class_getInstanceMethod(c, orig);
    Method newMethod = class_getInstanceMethod(c, new);
    if(class_addMethod(c, orig, method_getImplementation(newMethod), method_getTypeEncoding(newMethod)))
        class_replaceMethod(c, new, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    else
        method_exchangeImplementations(origMethod, newMethod);
}