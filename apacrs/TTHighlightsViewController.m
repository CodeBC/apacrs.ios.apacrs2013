//
//  TTHighlightsViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/30/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTHighlightsViewController.h"
#import "TTHighlightCell.h"
#import "TTProgramDetailViewController.h"
#import "Program.h"
#import "Program+Additions.h"
#import "TTFavourites.h"

@interface TTHighlightsViewController () <UITableViewDelegate>

@property (nonatomic, strong) NICellFactory *cellFactory;

@end

@implementation TTHighlightsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.cellFactory = [[NICellFactory alloc] init];
        [self.cellFactory mapObjectClass:[Program class] toCellClass:[TTHighlightCell class]];
        self.isBookmarked = NO;
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 42, 40)];
    [button setBackgroundImage:[UIImage imageNamed:@"Fav"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(bookmarkAll) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void)bookmarkAll
{

    NSArray *programsOnDisplay = [Program MR_findByAttribute:@"isProgramHighlighted" withValue:[NSNumber numberWithBool:YES] andOrderBy:@"order" ascending:YES];

    if (self.isBookmarked) {
        for (Program *program in programsOnDisplay) {
            [TTFavourites favourite:NO program:program];
        }
        self.isBookmarked = NO;
    } else {
        for (Program *program in programsOnDisplay) {
            [TTFavourites favourite:YES program:program];
        }
        self.isBookmarked = YES;
    }
    [self.tableView reloadData];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *items = [Program MR_findByAttribute:@"isProgramHighlighted" withValue:[NSNumber numberWithBool:YES] andOrderBy:@"order" ascending:YES];
    self.tableModel = [[NITableViewModel alloc] initWithListArray:items delegate:self.cellFactory];
    self.title = NSLocalizedString(@"Highlights", nil);
    
    NSMutableArray *programs = [TTFavourites favouritedPrograms];
    Program *program = [items objectAtIndex:1];
    if ([programs containsObject:program.id]) {
        self.isBookmarked = YES;
    } else {
        self.isBookmarked = NO;
    }
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITableViewModel *model = self.tableModel;
    TTProgramDetailViewController *programDetailViewController = [[TTProgramDetailViewController alloc] initWithNibName:nil
                                                                                                                bundle:nil];
    programDetailViewController.program = [model objectAtIndexPath:indexPath];
    [self.navigationController pushViewController:programDetailViewController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
