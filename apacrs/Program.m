//
//  Program.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 6/22/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Program.h"
#import "Location.h"
#import "Speaker.h"
#import "Time.h"
#import "Topic.h"


@implementation Program

@dynamic altName;
@dynamic date;
@dynamic desc;
@dynamic endTime;
@dynamic id;
@dynamic image;
@dynamic isHidden;
@dynamic isProgramHighlighted;
@dynamic keywords;
@dynamic order;
@dynamic programGroup;
@dynamic programGroupId;
@dynamic startTime;
@dynamic title;
@dynamic type;
@dynamic speakerOrder;
@dynamic topicOrder;
@dynamic location;
@dynamic speakers;
@dynamic time;
@dynamic topics;

@end
