//
//  Speaker.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FilmFestival, Freepaper, Poster, Program, Topic;

@interface Speaker : NSManagedObject

@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *programs;
@property (nonatomic, retain) Topic *topics;
@property (nonatomic, retain) NSSet *freepapers;
@property (nonatomic, retain) NSSet *posters;
@property (nonatomic, retain) NSSet *filmFestivals;
@end

@interface Speaker (CoreDataGeneratedAccessors)

- (void)addProgramsObject:(Program *)value;
- (void)removeProgramsObject:(Program *)value;
- (void)addPrograms:(NSSet *)values;
- (void)removePrograms:(NSSet *)values;

- (void)addFreepapersObject:(Freepaper *)value;
- (void)removeFreepapersObject:(Freepaper *)value;
- (void)addFreepapers:(NSSet *)values;
- (void)removeFreepapers:(NSSet *)values;

- (void)addPostersObject:(Poster *)value;
- (void)removePostersObject:(Poster *)value;
- (void)addPosters:(NSSet *)values;
- (void)removePosters:(NSSet *)values;

- (void)addFilmFestivalsObject:(FilmFestival *)value;
- (void)removeFilmFestivalsObject:(FilmFestival *)value;
- (void)addFilmFestivals:(NSSet *)values;
- (void)removeFilmFestivals:(NSSet *)values;

@end
