//
//  TTHighlightCell.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/30/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTTitleCell.h"
#import "Program.h"

@interface TTHighlightCell : TTTitleCell
@property (nonatomic, strong) UIButton *favouriteButton;
@property (nonatomic, strong) Program *program;
@property (nonatomic, assign) BOOL favourite;
@end
