//
//  TTPhotoAlbumViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/18/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTPhotoAlbumViewController : NIToolbarPhotoViewController

@property (nonatomic, strong) NSArray *photos;

@end
