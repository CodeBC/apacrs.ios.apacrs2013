//
//  TTFavourites.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/31/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

@class Program;

#import <Foundation/Foundation.h>

@interface TTFavourites : NSObject

+ (NSMutableArray *)favouritedPrograms;
+ (void)favourite:(BOOL)favourite program:(Program *)program;

@end
