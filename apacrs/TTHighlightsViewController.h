//
//  TTHighlightsViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/30/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTTableViewController.h"

@interface TTHighlightsViewController : TTTableViewController

@property (nonatomic, assign) BOOL isBookmarked;

@end
