//
//  Topic.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Topic.h"
#import "Freepaper.h"
#import "Program.h"
#import "Speaker.h"


@implementation Topic

@dynamic id;
@dynamic name;
@dynamic freepaper;
@dynamic program;
@dynamic speakers;
@dynamic speakerOrder;

@end
