//
//  TTFilmFestivalCell.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTFilmFestivalCell.h"
#import "FilmFestival.h"
#import "Speaker.h"
#import "TTAppTheme.h"

@implementation TTFilmFestivalCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = [TTAppTheme cellBackgroundColor];
        
        _posterNumberLabel = [[UILabel alloc] init];
        _titleLabel = [[UILabel alloc] init];
        _speakerLabel = [[UILabel alloc] init];
        
        _posterNumberLabel.font = [TTAppTheme regularCondensedFontWithSize:14];
        _titleLabel.font = [TTAppTheme mediumFontWithSize:14];
        _speakerLabel.font = [TTAppTheme regularFontWithSize:14];
        
        _titleLabel.textColor = [UIColor colorWithRed:0.004 green:0.545 blue:0.635 alpha:1.000];
        
        _posterNumberLabel.backgroundColor = [TTAppTheme cellSidebarColor];
        _posterNumberLabel.textColor = [UIColor whiteColor];
        _posterNumberLabel.textAlignment = NSTextAlignmentCenter;
        
        _titleLabel.backgroundColor = [UIColor clearColor];
        _speakerLabel.backgroundColor = [UIColor clearColor];
        
        [self.contentView addSubview:_posterNumberLabel];
        [self.contentView addSubview:_titleLabel];
        [self.contentView addSubview:_speakerLabel];
    }
    return self;
}

- (void)updateColorsForState:(BOOL)state
{
    [super updateColorsForState:state];
    
    _posterNumberLabel.backgroundColor = state ? [UIColor darkGrayColor] : [TTAppTheme cellSidebarColor];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat marginBetween = 5.0f;
    CGFloat posterNumberLabelWidth = 70.0f;
    
    CGFloat height = roundf(self.contentView.height/3);
    
    self.posterNumberLabel.frame = CGRectMake(0, 0, posterNumberLabelWidth, self.contentView.height);
    self.titleLabel.frame = CGRectMake(marginBetween + posterNumberLabelWidth, 0,
                                       self.contentView.width - (marginBetween + posterNumberLabelWidth),
                                       height*2);
    self.speakerLabel.frame = CGRectMake(self.titleLabel.left,
                                         self.titleLabel.height - 2,
                                         self.titleLabel.width,
                                         height*1);
}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    return 70.0f;
}


- (BOOL)shouldUpdateCellWithObject:(id)object
{
    if ([object isKindOfClass:[FilmFestival class]]) {
        
        FilmFestival *festival = (FilmFestival *)object;
        self.posterNumberLabel.text = festival.number;
        self.titleLabel.text = festival.title;
        self.speakerLabel.text = festival.speaker.name;
    }
    return YES;
}

@end
