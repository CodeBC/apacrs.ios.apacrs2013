//
//  TTNextMeetingViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTNextMeetingViewController : UIViewController

@property (nonatomic, strong) UIImageView *imageView;

@end
