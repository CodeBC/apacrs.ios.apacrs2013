//
//  TTSponsor.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/9/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTSponsor.h"

@implementation TTSponsor

+ (TTSponsor *)sponsorWithName:(NSString *)name address:(NSString *)address fax:(NSString *)fax tel:(NSString *)tel email:(NSString *)email web:(NSString *)web bannerImage:(UIImage *)bannerImage
{
    TTSponsor *sponsor = [[TTSponsor alloc] init];
    sponsor.name = name;
    sponsor.address = address;
    sponsor.fax = fax;
    sponsor.tel = tel;
    sponsor.email = email;
    sponsor.web = web;
    sponsor.bannerImage = bannerImage;
    return sponsor;
}

@end
