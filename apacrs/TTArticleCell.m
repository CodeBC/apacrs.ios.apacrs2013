//
//  TTArticleCell.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/19/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTArticleCell.h"
#import "Article.h"

#import "TTAppTheme.h"

@implementation TTArticleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [TTAppTheme cellBackgroundColor];
        self.textLabel.font = [TTAppTheme regularFontWithSize:14];
    }
    return self;
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    Article *article = (Article *)object;
    if ([article isKindOfClass:[Article class]]) {
        self.textLabel.text = article.title;
    }
    return YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.textLabel.height -= 1;
}

@end
