//
//  TTFreepaperCell.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTProgramCell.h"

@class Freepaper;

@interface TTFreepaperCell : TTProgramCell

@property (nonatomic, strong) Freepaper *freepaper;

@end
