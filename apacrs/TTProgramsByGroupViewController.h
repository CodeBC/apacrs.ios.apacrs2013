//
//  TTProgramsByGroupViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTProgramsViewController.h"

@interface TTProgramsByGroupViewController : TTProgramsViewController

@property (nonatomic, strong) NSNumber *groupId;

- (id)initWithGroupId:(NSNumber *)groupId;

@end
