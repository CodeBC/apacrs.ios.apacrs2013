//
//  Topic+Additions.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/28/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Topic.h"

@interface Topic (Additions)
- (NSComparisonResult)compare:(Topic *)anotherTopic usingSortOrder:(NSArray *)sortOrder;

@end