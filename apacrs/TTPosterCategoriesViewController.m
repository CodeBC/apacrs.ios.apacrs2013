//
//  TTPosterCategoriesViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/29/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTPosterCategoriesViewController.h"
#import "TTPostersViewController.h"
#import "TTTitleCell.h"
#import "Poster.h"
#import "Poster+Additions.h"

#import "TTAppTheme.h"

@interface TTPosterCategoriesViewController ()

@end

@implementation TTPosterCategoriesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.cellFactory mapObjectClass:[NITitleCellObject class] toCellClass:[TTTitleCell class]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Posters", nil);
    
    NSArray *categoryNames = [Poster categoryNames];
    NSMutableArray *listItems = [NSMutableArray new];
    
    for (NSString *category in categoryNames) {
        [listItems addObject:[[NITitleCellObject alloc] initWithTitle:category]];
    }
    
    self.tableModel = [[NITableViewModel alloc] initWithListArray:listItems delegate:self.cellFactory];
    self.tableView.delegate = self;
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITitleCellObject *titleObject = [self.tableModel objectAtIndexPath:indexPath];
    TTPostersViewController *postersViewController = [[TTPostersViewController alloc] initWithNibName:nil bundle:nil];
    postersViewController.category = titleObject.title;
    [self.navigationController pushViewController:postersViewController animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView * childView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 180)];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 240, 180)];
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.font = [TTAppTheme regularFontWithSize:15];
    textLabel.shadowColor = [UIColor colorWithWhite:1.0 alpha:0.8];
    textLabel.numberOfLines = 5;
    textLabel.textAlignment = NSTextAlignmentCenter;

    textLabel.text = @"Posters are available for viewing in the Exhibition Hall from 11-13 July 2013, 08:30 - 18:30hrs at Level 4, Suntec Convention Centre.";
    [childView addSubview:textLabel];
    
    return childView;
}


@end
