//
//  TTSponsorCellObject.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/9/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TTSponsor;

@interface TTSponsorCellObject : NICellObject

@property (nonatomic, strong) TTSponsor *sponsor;

- (id)initWithSponsor:(TTSponsor *)sponsor;

@end
