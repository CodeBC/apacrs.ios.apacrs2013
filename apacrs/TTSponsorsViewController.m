//
//  TTSponsorsViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/9/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTSponsorsViewController.h"
#import "TTSponsor.h"
#import "TTSponsorCellObject.h"
#import "TTSponsorDetailsViewController.h"

@interface TTSponsorsViewController () <UITableViewDelegate>

@property (nonatomic, strong) NITableViewModel *sponsorModel;

@end

@implementation TTSponsorsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                               0,
                                                               self.view.width,
                                                               self.view.height - self.navigationController.navigationBar.height)
                                              style:UITableViewStylePlain];
    _tableView.delegate = self;
    [self.view addSubview:_tableView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Sponsors", nil);
    
    TTSponsor *sponsor = [TTSponsor sponsorWithName:@"APACRS"
                                            address:@"Singapore National Eye Centre\n11 Third Hospital Avenue\nSingapore 168751"
                                                fax:@"+65 6327 8630"
                                                tel:@"+65 6322 7469"
                                              email:@"apacrs@snec.com.sg"
                                                web:@"http://www.apacrs.org"
                                        bannerImage:[UIImage imageNamed:@"topBanner"]];
    
    TTSponsor *sponsor2 = [TTSponsor sponsorWithName:@"Asia Cornea Society"
                                             address:@"Singapore National Eye Centre\n11 Third Hospital Avenue\nSingapore 168751"
                                                 fax:@"+65 6327 8630"
                                                 tel:@"+65 6322 7469"
                                               email:@"acs@snec.com.sg"
                                                 web:@"http://www.asiacorneasociety.org/"
                                         bannerImage:nil];
    
    TTSponsor *sponsor3 = [TTSponsor sponsorWithName:@"Cornea Society"
                                             address:nil
                                                 fax:nil
                                                 tel:nil
                                               email:nil
                                                 web:@"http://www.corneasociety.org/"
                                         bannerImage:nil];
    
    TTSponsor *sponsor4 = [TTSponsor sponsorWithName:@"ASCRS"
                                             address:@"4000 Legato Rd. Suite 700\nFairfax, VA ¥ 22033"
                                                 fax:@"(703) 591-0614"
                                                 tel:@"(703) 591-2220"
                                               email:@""
                                                 web:@"http://www.ascrs.org/"
                                         bannerImage:nil];
    
    NSArray *sponsors = @[sponsor, sponsor2, sponsor3, sponsor4];
    NSMutableArray *objects = [NSMutableArray new];
    for (TTSponsor *sponsor in sponsors) {
        [objects addObject:[[TTSponsorCellObject alloc] initWithSponsor:sponsor]];
    }
    
    _sponsorModel = [[NITableViewModel alloc] initWithListArray:objects
                                                       delegate:(id)[NICellFactory class]];
    self.tableView.dataSource = _sponsorModel;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [NICellFactory tableView:tableView heightForRowAtIndexPath:indexPath model:_sponsorModel];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTSponsorCellObject *object = (TTSponsorCellObject *)[_sponsorModel objectAtIndexPath:indexPath];
    TTSponsorDetailsViewController *viewController = [[TTSponsorDetailsViewController alloc] init];
    viewController.sponsor = object.sponsor;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
