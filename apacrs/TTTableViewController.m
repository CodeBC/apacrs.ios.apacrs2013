//
//  TTTableViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTTableViewController.h"
#import "TTAppTheme.h"

@interface TTTableViewController ()

@end

@implementation TTTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.cellFactory = [[NICellFactory alloc] init];
        self.clearsSelectionOnViewWillAppear = YES;
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:[self tableViewStyle]];
    self.tableView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        self.tableView.separatorInset = UIEdgeInsetsZero;
    }
    
    self.tableView.backgroundColor = [TTAppTheme backgroundColor];
    self.view.backgroundColor = [TTAppTheme backgroundColor];
}

- (UITableViewStyle)tableViewStyle
{
    return UITableViewStylePlain;
}

- (void)setTableModel:(NITableViewModel *)tableModel
{
    if (_tableModel != tableModel) {
        _tableModel = tableModel;
        self.tableView.dataSource = tableModel;
        [self.tableView reloadData];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITableViewModel *model = self.tableModel;
    
    id dataSource = tableView.dataSource;
    if ([dataSource isKindOfClass:[NITableViewModel class]]) {
        model = dataSource;
    }
    return [self.cellFactory tableView:tableView heightForRowAtIndexPath:indexPath model:model];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.clearsSelectionOnViewWillAppear) {
        for (NSIndexPath *indexPath in self.tableView.indexPathsForSelectedRows) {
            [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
