//
//  TTFloorPlanViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/16/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Location;

@interface TTFloorPlanViewController : NSObject

@property (nonatomic, strong) UIWebView *floorPlanView;

- (void)hilightLocation:(Location *)location;

@end
