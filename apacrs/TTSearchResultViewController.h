//
//  TTSearchResultViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/9/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTProgramsViewController.h"

@interface TTSearchResultViewController : TTProgramsViewController

@property (nonatomic, strong) NSArray *programs;

@end
