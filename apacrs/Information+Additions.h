//
//  Information+Additions.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Information.h"

@interface Information (Additions)

+ (NSArray *)groupNames;
- (Class)cellClass;

@end
