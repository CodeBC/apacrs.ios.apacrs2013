//
//  TTFavourites.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/31/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTFavourites.h"
#import "Program.h"

@implementation TTFavourites

+ (NSMutableArray *)favouritedPrograms
{
    static NSMutableArray *programs;
    if (programs) {
        return programs;
    }
    programs = [[NSUserDefaults standardUserDefaults] objectForKey:@"FavouritedPrograms"];
    if ([programs isKindOfClass:[NSMutableArray class]] == NO) {
        programs = [NSMutableArray new];
    }
    return programs;
}

+ (void)favourite:(BOOL)favourite program:(Program *)program
{
    NSMutableArray *programs = [self favouritedPrograms];
    if (favourite) {
        [programs addObject:program.id];
    } else {
        [programs removeObject:program.id];
    }
    [[NSUserDefaults standardUserDefaults] setObject:programs forKey:@"FavouritedPrograms"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
