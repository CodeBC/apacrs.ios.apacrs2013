//
//  TTProgramDetailViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/25/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTProgramDetailViewController.h"
#import "Program.h"
#import "Program+Additions.h"
#import "Time.h"
#import "Location.h"
#import "Speaker.h"
#import "Speaker+Additions.h"
#import "Topic.h"
#import "Topic+Additions.h"

#import "TTFavourites.h"

@interface TTProgramDetailViewController ()

@property (nonatomic, strong) UIButton *favButton;

@end

@implementation TTProgramDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.program) {
        [self loadHTML];
    }
}

#pragma mark -

- (BOOL)isProgramFaved
{
    BOOL faved = NO;
    
    NSMutableArray *programs = [TTFavourites favouritedPrograms];
    if ([programs containsObject:self.program.id]) {
        faved = YES;
    }
    
    return faved;
}

- (void)favThisProgram
{
    [TTFavourites favourite:![self isProgramFaved] program:self.program];
    [_favButton setSelected:[self isProgramFaved]];
}

- (void)updateFavButton
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0, 0, 42, 40)];
    [button setImage:[UIImage imageNamed:@"Fav"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"FavedFilled"] forState:UIControlStateSelected];
    [button addTarget:self action:@selector(favThisProgram) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    [button setSelected:[self isProgramFaved]];
    
    _favButton = button;
}

#pragma mark -
#pragma mark UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [super webViewDidFinishLoad:webView];
    self.title = self.program.title;
    
    [self updateFavButton];
}

#pragma mark - 
#pragma mark Program

- (void)setProgram:(Program *)program
{
    if (_program != program) {
        _program = program;
    }
    if (_webView) {
        [self loadHTML];
    }
}

- (NSString *)HTMLFileName
{
    return @"session";
}

- (NSDictionary *)replaceValues
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMM"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSString *speakerFormat = @"<span>%@</span>";
    NSArray *speakerOrder = [self.program.speakerOrder componentsSeparatedByString:@","];
    NSArray *speakers = [self.program.speakers.allObjects sortedArrayWithOptions:NSSortConcurrent
                                                                 usingComparator:^NSComparisonResult(Speaker *s1, Speaker *s2) {
                                                                     return [s1 compare:s2 usingSortOrder:speakerOrder];
    }];
    NSString *speakerList = [[speakers speakerNamesWithFormatString:speakerFormat] componentsJoinedByString:@"\n"];
    
    NSMutableArray *topicAndSpeakerList = [NSMutableArray new];
    
    NSArray *topicOrder = [self.program.topicOrder componentsSeparatedByString:@","];
    NSArray *sorted = [self.program.topics.allObjects sortedArrayWithOptions:NSSortConcurrent
                                                               usingComparator:^NSComparisonResult(Topic *s1, Topic *s2) {
                                                                   return [s1 compare:s2 usingSortOrder:topicOrder];}];
    
    for (Topic *topic in sorted) {
        NSArray *topicSpeakerOrder = [topic.speakerOrder componentsSeparatedByString:@","];
        NSArray *topicSpeakers = [topic.speakers.allObjects sortedArrayWithOptions:NSSortConcurrent
                                                                   usingComparator:^NSComparisonResult(Speaker *s1, Speaker *s2) {
                                                                       return [s1 compare:s2 usingSortOrder:topicSpeakerOrder];
                                                                   }];
        NSString *topicSpeakerList = [[topicSpeakers speakerNamesWithFormatString:speakerFormat] componentsJoinedByString:@"\n"];
        
        NSString *topicAndSpeaker = [NSString stringWithFormat:@"<li><h5 class=\"title\">%@</h5>%@</li>",
                                     topic.name,
                                     topicSpeakerList];
        [topicAndSpeakerList addObject:topicAndSpeaker];
    }
    
    NSString *topicAndSpeaker = [topicAndSpeakerList componentsJoinedByString:@"\n"];
    
    NSMutableDictionary *values = [@{
                                      @"Program-Title": self.program.title ? self.program.title : @"",
                                      @"Program-Description": self.program.desc ? self.program.desc : @"",
                                      @"Program-Date": self.program.date ? [formatter stringFromDate:self.program.date] : @"",
                                      @"Program-Time": self.program.programTime,
                                      @"Program-Speakers": speakerList.length ? speakerList : @"<span></span>",
                                      @"Topics-Speakers": topicAndSpeaker.length ? topicAndSpeaker : @"<li><span></span></li>"
                                      } mutableCopy];
    
    if (self.program.location.name) {
        values[@"Program-Location"] = self.program.location.name;
    } else {
        values[@"Program-Location"] = @"";
    }
    
    return values;
}

@end
