//
//  Location.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/25/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Location.h"
#import "NSArray+TTAddition.h"

@implementation Location

@dynamic id;
@dynamic name;
@dynamic programs;

@end

@implementation NSArray (LocationGrouping)

- (NSArray *)groupedLocationByName
{
    return [self groupedByKeyPath:@"name"];
}

@end