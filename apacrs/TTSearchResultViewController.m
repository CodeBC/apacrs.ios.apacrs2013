//
//  TTSearchResultViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/9/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTSearchResultViewController.h"
#import "SVSegmentedControl.h"
#import "TTProgramsViewController.h"
#import "NSString+DateFormatAddition.h"
#import "NSArray+TTAddition.h"

#import "Program.h"
#import "Program+Additions.h"
#import "Speaker.h"
#import "Topic.h"
#import "Time.h"
#import "Time+Additions.h"

@interface TTSearchResultViewController ()

@end

@implementation TTSearchResultViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Search Result", nil);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSArray *)allPrograms
{
    return self.programs;
}

- (NSArray *)dateTitles
{
    if ([self isEmpty]) {
        return @[NSLocalizedString(@"N/A", nil)];
    }
    NSArray *dateTitles = [[self allPrograms] dateTitles];
    
    return dateTitles;
}

- (NSArray *)timeTitles
{
    if ([self isEmpty]) {
        return @[NSLocalizedString(@"N/A", nil)];
    }
    NSString *dateTitle = self.dateChooser.sectionTitles[self.dateChooser.selectedSegmentIndex];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self IN %@ AND %K = %@",
                              [self allPrograms],
                              @"date", [dateTitle dateValue]];
    NSArray *programs = [Program MR_findAllWithPredicate:predicate];
    NSArray *times = [programs arrayOfKeyPath:@"time"];
    
    return [times timeTitles];
}

- (NSString *)emptyText
{
    return NSLocalizedString(@"No programs found", nil);
}

- (CGRect)emptyViewFrame
{
    return self.view.bounds;
}

- (BOOL)isEmpty
{
    return [self allPrograms].count == 0;
}

- (NSArray *)programsForSelectedDateAndTime
{
    if ([self isEmpty]) {
        return @[];
    }
    NSString *dayTitle = self.dateChooser.sectionTitles[self.dateChooser.selectedSegmentIndex];
    NSString *timeTitle = self.timeChooser.sectionTitles[self.timeChooser.selectedSegmentIndex];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self IN %@ AND %K = %@ AND ANY %K = %@ AND %K = %@",
                              [self allPrograms],
                              @"date", [dayTitle dateValue],
                              @"time.name", timeTitle,
                              @"isHidden", [NSNumber numberWithBool:NO]];
    NSArray *events = [Program MR_findAllWithPredicate:predicate];
    return events;
}

@end
