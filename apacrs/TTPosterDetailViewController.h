//
//  TTPosterDetailViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/29/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTHTMLTemplateViewController.h"

@class Poster;

@interface TTPosterDetailViewController : TTHTMLTemplateViewController

@property (nonatomic, strong) Poster *poster;

@end
