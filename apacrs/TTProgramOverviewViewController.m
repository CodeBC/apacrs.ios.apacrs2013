//
//  TTProgramOverviewViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 6/2/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTProgramOverviewViewController.h"
#import "TTProgramDetailViewController.h"
#import "Program.h"

@interface TTProgramOverviewViewController () <UIWebViewDelegate>

@end

@implementation TTProgramOverviewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    _floorPlanView = [[UIWebView alloc] initWithFrame:CGRectZero];
    _floorPlanView.frame = CGRectMake(0,
                                      0,
                                      self.view.width,
                                      self.view.height);
    _floorPlanView.delegate = self;
    _floorPlanView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:_floorPlanView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Program Overview", nil);
    
    NSURL *htmlURL = [[NSBundle mainBundle] URLForResource:@"ProgramOverview/program_overview" withExtension:@"html"];
    [_floorPlanView loadRequest:[NSURLRequest requestWithURL:htmlURL]];
}

#pragma mark - 
#pragma mark UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL *URL = request.URL;
    if ([URL.scheme isEqualToString:@"native"]) {
        if ([URL.host isEqualToString:@"program"]) {
            NSString *programId = [URL.query substringFromIndex:[@"id=" length]];
            
            Program *program = [Program MR_findFirstByAttribute:@"id" withValue:programId];
            if (program) {
                TTProgramDetailViewController *programDetailViewController = [[TTProgramDetailViewController alloc] initWithNibName:nil bundle:nil];
                programDetailViewController.program = program;
                [self.navigationController pushViewController:programDetailViewController animated:YES];
            }
        }
    }
    return YES;
}

@end
