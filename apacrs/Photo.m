//
//  Photo.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/18/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Photo.h"


@implementation Photo

@dynamic id;
@dynamic title;
@dynamic url;
@dynamic album;

@end
