//
//  Venue.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 4/8/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Venue : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * exhibitorName;
@property (nonatomic, retain) NSString * boothNo;

@end

@interface NSArray (VenueGrouping)

- (NSArray *)groupedVenueByName;

@end
