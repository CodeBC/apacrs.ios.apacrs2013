//
//  TTFilmFestivalCell.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTTitleCell.h"

@interface TTFilmFestivalCell : TTTitleCell

@property (nonatomic, strong) UILabel *posterNumberLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *speakerLabel;

@end
