//
//  Topic+Additions.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/28/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Topic+Additions.h"

@implementation Topic (Additions)
- (NSComparisonResult)compare:(Topic *)anotherTopic usingSortOrder:(NSArray *)sortOrder
{
    NSString *sid1 = [self.id stringValue];
    NSString *sid2 = [anotherTopic.id stringValue];
    if ([sortOrder containsObject:sid1] && [sortOrder containsObject:sid2]) {
        if ([sortOrder indexOfObject:sid1] < [sortOrder indexOfObject:sid2]) {
            return NSOrderedAscending;
        } else if ([sortOrder indexOfObject:sid1] > [sortOrder indexOfObject:sid2]) {
            return NSOrderedDescending;
        } else {
            return NSOrderedSame;
        }
    }
    return NSOrderedSame;
}

@end
