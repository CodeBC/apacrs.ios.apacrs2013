//
//  TTScrollingBannersView.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 8/9/14.
//  Copyright (c) 2014 myOpenware. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TTScrollingBannersView;

@protocol TTScrollingBannersViewDelegate <NSObject>

@optional

- (void)scrollingBannerView:(TTScrollingBannersView *)bannerView didTapBannerAtIndex:(NSUInteger)bannerIndex;

@end

@interface TTScrollingBannersView : UICollectionReusableView

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSArray *bannerImages;
@property (nonatomic, assign) id<TTScrollingBannersViewDelegate> delegate;

@end
