//
//  TTPhotoAlbumViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/18/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTPhotoAlbumViewController.h"
#import "Photo.h"

@interface TTPhotoAlbumViewController () <NIPhotoAlbumScrollViewDataSource, NIPhotoAlbumScrollViewDelegate>

@property (nonatomic, strong) UIImage *originalNavBackground;

@end

@implementation TTPhotoAlbumViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    
    self.photoAlbumView.loadingImage = [UIImage imageWithContentsOfFile:
                                        NIPathForBundleResource(nil, @"NimbusPhotos.bundle/gfx/default.png")];
    
    self.photoAlbumView.dataSource = self;
    self.photoAlbumView.photoViewBackgroundColor = [UIColor blackColor];
    
    self.animateMovingToNextAndPreviousPhotos = YES;
    
    self.originalNavBackground = [self.navigationController.navigationBar backgroundImageForBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];
    [self.toolbar setTintColor:[UIColor blackColor]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:self.originalNavBackground forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.photoAlbumView reloadData];
    [self setChromeTitle];
}

#pragma mark - NIPhotoAlbumScrollViewDataSource

- (NSInteger)numberOfPagesInPagingScrollView:(NIPagingScrollView *)pagingScrollView
{
    return self.photos.count;
}

- (UIView<NIPagingScrollViewPage> *)pagingScrollView:(NIPagingScrollView *)pagingScrollView pageViewForIndex:(NSInteger)pageIndex
{
    UIView<NIPagingScrollViewPage> *pageView = [self.photoAlbumView pagingScrollView:pagingScrollView pageViewForIndex:pageIndex];
    return pageView;
}

- (UIImage *)photoAlbumScrollView: (NIPhotoAlbumScrollView *)photoAlbumScrollView
                     photoAtIndex: (NSInteger)photoIndex
                        photoSize: (NIPhotoScrollViewPhotoSize *)photoSize
                        isLoading: (BOOL *)isLoading
          originalPhotoDimensions: (CGSize *)originalPhotoDimensions
{
    Photo *photo = self.photos[photoIndex];
    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:photo.url]
                                                          options:SDWebImageDownloaderUseNSURLCache
                                                         progress:nil
                                                        completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                                            [photoAlbumScrollView didLoadPhoto:image
                                                                                       atIndex:photoIndex
                                                                                     photoSize:NIPhotoScrollViewPhotoSizeOriginal];
    }];
    
    *isLoading = YES;
    *photoSize = NIPhotoScrollViewPhotoSizeUnknown;
    *originalPhotoDimensions = CGSizeZero;

    return nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
