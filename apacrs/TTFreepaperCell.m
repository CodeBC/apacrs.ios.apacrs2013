//
//  TTFreepaperCell.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/5/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTFreepaperCell.h"
#import "Freepaper.h"
#import "Freepaper+Additions.h"
#import "Location.h"
#import "TTFavourites.h"

@implementation TTFreepaperCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.accessoryView = nil;
    }
    return self;
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    if ([object isKindOfClass:[Freepaper class]]) {
        
        self.freepaper = (Freepaper *)object;
        self.topicLabel.text = [NSString stringWithFormat:@"%@", self.freepaper.title];
        self.hallLabel.text = self.freepaper.location.name;
        self.timeLabel.text = self.freepaper.programTime;
        
    }
    return YES;
}

@end
