//
//  Freepaper.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 6/21/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "Freepaper.h"
#import "Location.h"
#import "Speaker.h"
#import "Time.h"
#import "Topic.h"


@implementation Freepaper

@dynamic date;
@dynamic endTime;
@dynamic id;
@dynamic startTime;
@dynamic title;
@dynamic isHidden;
@dynamic location;
@dynamic speakers;
@dynamic time;
@dynamic topics;

@end
