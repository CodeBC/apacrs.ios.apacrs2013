//
//  TTAlbumCell.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/18/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTAlbumCell.h"
#import "Album.h"

#import "TTAppTheme.h"

@implementation TTAlbumCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [TTAppTheme cellBackgroundColor];
        self.textLabel.font = [TTAppTheme regularFontWithSize:14];
    }
    return self;
}

- (BOOL)shouldUpdateCellWithObject:(id)object
{
    Album *album = (Album *)object;
    if ([album isKindOfClass:[Album class]]) {
        self.textLabel.text = album.title;
    }
    return YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.textLabel.height -= 1;
}

@end
