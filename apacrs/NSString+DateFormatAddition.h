//
//  NSString+DateFormatAddition.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/25/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DateFormatAddition)

- (NSDate *)dateValue;

@end
