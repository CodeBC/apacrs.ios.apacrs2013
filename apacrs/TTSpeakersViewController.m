//
//  TTSpeakersViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/16/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTSpeakersViewController.h"
#import "TTSpeakerViewController.h"
#import "Speaker.h"
#import "Speaker+Additions.h"
#import "TTSpeakerCell.h"

#import "TTAppTheme.h"

@interface TTSpeakersViewController () <UISearchDisplayDelegate, UITableViewDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) UISearchDisplayController *retainSearchDisplayController;
@property (nonatomic, strong) NITableViewModel *searchDataSouce;
@property (nonatomic, strong) NITableViewModel *listDataSouce;

@end

@implementation TTSpeakersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.cellFactory mapObjectClass:[Speaker class] toCellClass:[TTSpeakerCell class]];
    }
    return self;
}

- (void)chooseSortingMode:(id)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Sort By", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Name", nil), NSLocalizedString(@"Country", nil), nil];
    [sheet showFromBarButtonItem:sender animated:YES];
}

- (NSArray *)speakers
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K != 'No Country' AND %K > 0 AND %K > 0",
                              @"country",
                              @"country.length",
                              @"imageURL.length"];
    
    return [Speaker MR_findAllWithPredicate:predicate];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSArray *speakers = [self speakers];
    
    switch (buttonIndex) {
        case 0:
            _listDataSouce = [[NITableViewModel alloc] initWithSectionedArray:[speakers groupedSpeakerByName]
                                                                     delegate:self.cellFactory];
            [_listDataSouce setSectionIndexType:NITableViewModelSectionIndexAlphabetical showsSearch:NO showsSummary:YES];
            self.tableView.dataSource = _listDataSouce;
            break;
        case 1:
            _listDataSouce = [[NITableViewModel alloc] initWithSectionedArray:[speakers groupedSpeakerByCountry]
                                                                     delegate:self.cellFactory];
            [_listDataSouce setSectionIndexType:NITableViewModelSectionIndexAlphabetical showsSearch:NO showsSummary:YES];
            self.tableView.dataSource = _listDataSouce;
            break;
            
        default:
            break;
    }
    [_listDataSouce setSectionIndexType:NITableViewModelSectionIndexAlphabetical showsSearch:YES showsSummary:YES];
    [self.tableView reloadData];
}

- (void)loadView
{
    [super loadView];
    
    // Search Bar
    UISearchBar *searchBar = [[UISearchBar alloc] init];
    if ([searchBar respondsToSelector:@selector(setSearchBarStyle:)]) {
        searchBar.searchBarStyle = UISearchBarStyleMinimal;
    }

    [searchBar sizeToFit];
    _retainSearchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    _retainSearchDisplayController.delegate = self;
    _retainSearchDisplayController.searchResultsDelegate = self;
    self.tableView.tableHeaderView = searchBar;
    NSArray *speakers = [self speakers];
    
    _listDataSouce = [[NITableViewModel alloc] initWithSectionedArray:[speakers groupedSpeakerByName]
                                                             delegate:self.cellFactory];
    [_listDataSouce setSectionIndexType:NITableViewModelSectionIndexAlphabetical showsSearch:YES showsSummary:YES];
    
    self.tableModel = _listDataSouce;
    
    if ([self.tableView respondsToSelector:@selector(setTintColor:)]) {
        self.tableView.tintColor = [TTAppTheme darkColor];
    }
    
    UIBarButtonItem *sortItem = [[UIBarButtonItem alloc] initWithTitle:@"Sort"
                                                                 style:UIBarButtonItemStyleBordered
                                                                target:self
                                                                action:@selector(chooseSortingMode:)];
    self.navigationItem.rightBarButtonItem = sortItem;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Speakers", nil);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pushSpeakerViewControllerWithSpeaker:(Speaker *)speaker
{
    TTSpeakerViewController *speakerViewController = [[TTSpeakerViewController alloc] init];
    speakerViewController.speaker = speaker;
    [self.navigationController pushViewController:speakerViewController animated:YES];
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NITableViewModel *model = (NITableViewModel *)tableView.dataSource;
    [self.searchDisplayController setActive:NO animated:YES];
    [self pushSpeakerViewControllerWithSpeaker:[model objectAtIndexPath:indexPath]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.cellFactory tableView:tableView heightForRowAtIndexPath:indexPath model:tableView.dataSource];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    headerView.backgroundColor = [TTAppTheme tableSectionHeaderColor];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectInset(headerView.bounds, 8, 3)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.font = [TTAppTheme mediumFontWithSize:14];
    label.text = [tableView.dataSource tableView:tableView titleForHeaderInSection:section];
    
    [headerView addSubview:label];
    
    return headerView;
}

#pragma mark -
#pragma mark UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"%K contains[cd] %@",
                              @"name", searchString];
    NSArray *speakers = [[self speakers] filteredArrayUsingPredicate:predicate];
    
    _searchDataSouce = [[NITableViewModel alloc] initWithSectionedArray:[speakers groupedSpeakerByName]
                                                               delegate:self.cellFactory];
    self.searchDisplayController.searchResultsDataSource = _searchDataSouce;
    
    return YES;
}

@end
