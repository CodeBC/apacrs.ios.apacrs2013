//
//  TTMeetingReportDetailViewController.h
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTWebViewController.h"

@interface TTMeetingReportDetailViewController : TTWebViewController

@property (nonatomic, strong) NSURLRequest *request;

@end
