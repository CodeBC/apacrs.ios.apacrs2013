//
//  NSString+DateFormatAddition.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 3/25/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "NSString+DateFormatAddition.h"

@implementation NSString (DateFormatAddition)

- (NSDate *)dateValue
{
    static NSDateFormatter *formatter;
    if (!formatter) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [formatter setDateFormat:@"dd MMM (EEE) yyyy"];
    }
    
    NSDate *date = [formatter dateFromString:[NSString stringWithFormat:@"%@ 2013", self]];
    NSAssert(date, @"Date is null, check date format");
    return date;
}

@end
