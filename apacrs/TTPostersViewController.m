//
//  TTPostersViewController.m
//  apacrs
//
//  Created by Thant Thet Khin Zaw on 5/6/13.
//  Copyright (c) 2013 myOpenware. All rights reserved.
//

#import "TTPostersViewController.h"
#import "Poster.h"
#import "TTPosterCell.h"
#import "TTPosterDetailViewController.h"

@interface TTPostersViewController ()

@end

@implementation TTPostersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self.cellFactory mapObjectClass:[Poster class] toCellClass:[TTPosterCell class]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Posters", nil);
    
    NSArray *items = [Poster MR_findByAttribute:@"category" withValue:self.category andOrderBy:@"number" ascending:YES];
    self.tableModel = [[NITableViewModel alloc] initWithListArray:items delegate:self.cellFactory];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Poster *item = [self.tableModel objectAtIndexPath:indexPath];
    TTPosterDetailViewController *posterDetailView = [[TTPosterDetailViewController alloc] init];
    posterDetailView.poster = item;
    [self.navigationController pushViewController:posterDetailView animated:YES];
}

@end
